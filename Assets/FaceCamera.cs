﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var cam = Camera.main.transform;

        var dir = transform.position - cam.position;
        transform.forward = dir;
    }
}
