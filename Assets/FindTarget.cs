﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindTarget : MonoBehaviour
{
    public Transform _milk;
    public Transform _catLady;
    public Transform _exit;

    CatSystem catSystem;

    // Start is called before the first frame update
    void Start()
    {
        catSystem = GetComponentInParent<CatSystem>();    
    }


    private void OnTriggerEnter(Collider other)
    {
       
            CatBowl c = other.GetFromHeirarchy<CatBowl>();

            if (c)
            {
                if(c.Isfull)
                {
                    c.Empty();

                if(catSystem)
                    catSystem.FeedMe();
                }
            }

                
    }

    private void OnTriggerExit(Collider other)
    {
    //    if (other.CompareTag("CatLady"))
    //        _catLady = null;

    //    if (other.CompareTag("Milk"))
    //    {
    //        _milk = null;
    //    }

    //    if (other.CompareTag("Exit"))
    //    {
    //        _exit = null;
    //    }

    }
}
