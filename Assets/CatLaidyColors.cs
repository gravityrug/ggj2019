﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatLaidyColors : MonoBehaviour
{
    public Color[] HairColor;
    public Color[] DressColor;

    ControllerInput input;

    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<ControllerInput>();

        var Hair = this.GetAllFromHeirarchy<Hair>();
        var Dress = this.GetAllFromHeirarchy<Dress>();

        foreach (var item in Hair)
        {
            item.GetComponent<Renderer>().material.color = HairColor[(int)input.controller];
        }
        foreach (var item in Dress)
        {
            item.GetComponent<Renderer>().material.color = DressColor[(int)input.controller];
        }

    }
}
