﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatLadyInputSystem : MonoBehaviour
{
    ControllerInput input;
    CatLadyData data;

    public KeyCode Pickup;
    public KeyCode Dash;

    // Start is called before the first frame update
    void Start()
    {
        data = this.GetFromHeirarchy<CatLadyData>();
        
        input = this.GetFromHeirarchy<ControllerInput>();

        if (input.controller == Inputs.PlayerIndex.One)
            return;
        if (!input.Connected)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        data.InputX = Input.GetAxis("Horizontal");
        data.InputY = Input.GetAxis("Vertical");
        data.InputDash = SetButton(data.InputDash, Input.GetKey(Dash));
        data.InputPickUp = SetButton(data.InputPickUp, Input.GetKey(Pickup));
        if (input.Connected)
        {
            data.InputX = input.GetAxis(Inputs.Axis.LeftStickX);
            data.InputY = input.GetAxis(Inputs.Axis.LeftStickY);
            data.InputDash = input.GetButton(Inputs.Buttons.Square);
            data.InputPickUp = input.GetButton(Inputs.Buttons.Circle);
        }
    }

    Inputs.State SetButton(Inputs.State previous, bool pressed)
    {
        if (pressed)
        {
            if (previous == Inputs.State.None)
                return Inputs.State.Pushed;
            if (previous == Inputs.State.Pushed || previous == Inputs.State.Held)
                return Inputs.State.Held;
            return Inputs.State.None;
        }
        if (previous == Inputs.State.Pushed || previous == Inputs.State.Held)
        {
            return Inputs.State.Released;
        }
        return Inputs.State.None;
        

    }

}
