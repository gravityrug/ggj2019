﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public Color[] Colors;

    // Start is called before the first frame update
    void Start()
    {

        foreach (var item in transform.root.GetComponentsInChildren<Renderer>())
        {
            item.material.color = Colors[Random.Range(0, Colors.Length)];
        }
    }    
}
