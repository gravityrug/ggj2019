﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fridge : MonoBehaviour
{
    public AudioAsset fridgeSounds;
    Transform t;

    private void Start()
    {
        t = Camera.main.transform;
    }

    public Fridge GetMilk()
    {
        this.GetFromHeirarchy<Animator>().CrossFade("GotMilk", .2f);
        AudioSource.PlayClipAtPoint(fridgeSounds, t.position);
        return this;
    }
}
