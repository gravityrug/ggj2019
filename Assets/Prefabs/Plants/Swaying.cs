﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swaying : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartRotation = transform.eulerAngles;
        Offset = Random.Range(0f, 10f);
    }

    Vector3 StartRotation;

    public float Offset = 0f;
    public float SwayAmount = 5f;
    public float SwaySpeed = 1f;

    // Update is called once per frame
    void Update()
    {
        var rot = transform.eulerAngles;
        rot.x = StartRotation.x + Mathf.Sin(Offset + Time.time * SwaySpeed) * SwayAmount;
        transform.eulerAngles = rot;
    }
}
