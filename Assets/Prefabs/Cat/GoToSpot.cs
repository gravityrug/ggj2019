﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToSpot : MonoBehaviour
{
    public GameObject target;

    NavigatorInfo info;

    // Start is called before the first frame update
    void Start()
    {
        Navigator.StartUpdates(info);
        info.goalPosition = target.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        info.currentPosition = transform.position;
        info.goalPosition = target.transform.position;

        if (!info.AtDestination)
        {
            transform.forward =  info.DirectionToNextPosition;
            transform.position += transform.forward * Time.deltaTime * 2f;
        }
    }
}
