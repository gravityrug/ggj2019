﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeText : MonoBehaviour
{
    [Range(0f, 1f)]
    public float FadeAmount;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        col = mat.color;// ("_TextColor");
    }

    Color col;
    Material mat;

    // Update is called once per frame
    void Update()
    {
        col.a = 1f-FadeAmount;
        mat.color = col;//.SetColor("_TextColor", col);
    }
}
