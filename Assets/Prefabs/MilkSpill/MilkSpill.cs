﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MilkSpill : MonoBehaviour
{
    private void OnEnable()
    {
        //transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        elapsedTime = 0;
    }
    public float killTime = 1;
    float elapsedTime;

    private void Update()
    {
        elapsedTime += Time.deltaTime;
        if (elapsedTime > killTime)
            Destroy(transform.root.gameObject);
    }
}
