﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatBowl : MonoBehaviour
{
    public Vector3 EmptyPosition;
    public Vector3 FullPosition;
    public Transform Milk;
    public Rigidbody rb;
    public float SpillAngle = 60f;

    public GameObject MilkSpill;

    public bool Isfull;

    private void Start()
    {
        rb = this.GetFromHeirarchy<Rigidbody>();
    }

    public void Fill()
    {
        Milk.transform.localPosition = FullPosition;
        Isfull = true;
    }

    public void Empty()
    {
        Milk.transform.localPosition = EmptyPosition;
        Isfull = false;
    }

    private void Update()
    {
        if (Isfull && Vector3.Angle(transform.up, Vector3.up) > SpillAngle)
        {
            Instantiate(MilkSpill).transform.position = transform.position;
            Empty();
        }
    }
}
