using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CatLady
{
    namespace Conditions
    {
	    public class HoldingObject : Condition
	    {
            CatLadyData Data;

            public bool isHoldingObject;

            private void Start()
            {
                Data = this.GetFromHeirarchy<CatLadyData>();
            }

            public override bool Evaluate()
            {
                return Data.HeldObject == isHoldingObject;
            }
        }
    }
}


