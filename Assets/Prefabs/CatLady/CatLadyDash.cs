using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateBehaviours.CatLady
{
	public class CatLadyDash : StateBehaviour, IEnterState, IFixedState
	{
        public AnimationCurve moveCurve;

        public float speed = 10f;

        Rigidbody rb;

        void Start()
        {
            rb = this.GetFromHeirarchy<Rigidbody>();
        }

        public void FixedUpdateState()
        {
            rb.velocity = rb.transform.forward * speed * moveCurve.Evaluate(Time.time - enterTime);
        }

        float enterTime;

        Vector3 vel;

        public void EnterState()
        {
            enterTime = Time.time;
            vel = rb.velocity.normalized;
        }
    }
}


