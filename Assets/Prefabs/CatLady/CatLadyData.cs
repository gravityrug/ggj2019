﻿using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;

namespace Events
{
    public struct AddPossiblePickup { public GameObject HeldObject; }
    public struct RemovePossiblePickup { public GameObject HeldObject; }
}

public class CatLadyData : MonoBehaviour
{
    public GameObject PossiblePickup;
    public GameObject HeldObject;
    public GameObject HeldPosition;
    public Vector3 ThrowForce;
    public int _PlayerID;
    public Inputs.State InputPickUp;
    public Inputs.State InputDash;
    public float InputX;
    public float InputY;
    public followcam _cam;

    ControllerInput _controller;

    private void Start()
    {
        _controller = GetComponent<ControllerInput>();
    }
    public float InputTilt
    {
        get
        {
            return new Vector2(InputX * Mathf.Sqrt(1 - .5f * InputY * InputY), InputY * Mathf.Sqrt(1 - .5f * InputX * InputX)).magnitude;
        }
    }
    public float InputTiltViewer;
    private void Update()
    {
        InputTiltViewer = InputTilt;

        if (_cam != null)
        {
            float camZ = _controller.GetAxis(Inputs.Axis.RightTrigger);
            _cam.CamZoom(Mathf.Lerp(10, 5, camZ));
        }
    }
}
