﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeldObject : MonoBehaviour
{
    public Type HeldObjectType;

    public Action DroppedByCatLady = delegate { };
    
    /// <summary>
    /// Transform is Held Position on the Cat Lady
    /// </summary>
    public Action<Transform> PickedUpByCatLady = delegate { };

    public enum Type
    {
        Cat,
        CatCannon
    }

    public Transform HeldPosition;
    Rigidbody rb;

    void Start()
    {
        rb = this.GetFromHeirarchy<Rigidbody>();

        PickedUpByCatLady += t =>
        {
            HeldPosition = t;
            rb.isKinematic = true;
        };
        DroppedByCatLady += () =>
        {
            rb.isKinematic = false;
            HeldPosition = null;
        };
    }

    private void OnDestroy()
    {
        PickedUpByCatLady = null;
        DroppedByCatLady = null;
    }

    private void LateUpdate()
    {
        if (HeldPosition && rb)
        {
            rb.position = HeldPosition.position;
            rb.rotation = HeldPosition.rotation;
        }
    }
}
