﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CatLady
{
    public class CatLadyMovement : StateBehaviour, IFixedState, IEnterState
    {
        [Range(0f, 20f)]
        public float speed = 0;

        public float rotationSpeed = 10f;

        public float inertia = .5f;

        Rigidbody rb;
        CatLadyData data;
        Camera cam;

        void Start()
        {
            rb = this.GetFromHeirarchy<Rigidbody>();
            data = this.GetFromHeirarchy<CatLadyData>();
            cam = Camera.main;
        }

        public void FixedUpdateState()
        {
            var CamForward = cam.transform.forward;
            CamForward.y = 0;
            CamForward.Normalize();

            var CamRight = cam.transform.right;
            CamRight.y = 0;
            CamRight.Normalize();

            var moveDir = CamRight * data.InputX + CamForward * data.InputY;

            rb.velocity = Vector3.Lerp(enterVelocity, moveDir.normalized * speed, (Time.time - enterTime)/inertia);

            //rb.velocity = moveDir.normalized * speed;

            //var lookDir = CamForward * input.GetAxis(Inputs.Axis.RightStickY) + CamRight * input.GetAxis(Inputs.Axis.RightStickX);

            var lookDir = rb.velocity;

            if (lookDir.magnitude > .1f)
                rb.MoveRotation(Quaternion.Lerp(rb.rotation, Quaternion.LookRotation(lookDir), Time.deltaTime * rotationSpeed));
        }

        float enterTime;
        Vector3 enterVelocity;

        public void EnterState()
        {
            enterVelocity = rb.velocity;
            enterTime = Time.time;
        }
    }

}
