using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace CatLady
{
	public class PickupOrDropHeldItem : StateBehaviour, IEnterState
	{
        CatLadyData data;
        Rigidbody rb;
        float forceAmount;

        void Start()
        {
            data = this.GetFromHeirarchy<CatLadyData>();
            rb = this.GetFromHeirarchy<Rigidbody>();
        }

		public void EnterState ()
		{
            if (data.HeldObject)
            {
                data.HeldObject.GetFromHeirarchy<Rigidbody>().velocity += rb.velocity;

                Vector3 force = data.HeldPosition.transform.forward.normalized;
                force.y =1f;
                force.Scale(data.ThrowForce);
                var CatSystem = data.HeldObject.GetFromHeirarchy<CatSystem>();
                if (CatSystem)
                    CatSystem._lastplayer = data._PlayerID;
              
                data.PossiblePickup = data.HeldObject;

               
                data.HeldObject.GetFromHeirarchy<HeldObject>().DroppedByCatLady();
                data.HeldObject.GetFromHeirarchy<Rigidbody>().AddForce(force, ForceMode.Impulse);
                data.HeldObject = null;
                
            }
            else if (data.PossiblePickup)
            {
                data.PossiblePickup.GetFromHeirarchy<HeldObject>().PickedUpByCatLady(data.HeldPosition.transform);
             
                data.HeldObject = data.PossiblePickup;
                data.HeldObject.GetFromHeirarchy<MeshRenderer>().transform.DOPunchScale(new Vector3(40.0f, 40.0f, 40.0f), 0.5f, 3, 1.0f);
                data.PossiblePickup = null;
            }
		}
	}
}


