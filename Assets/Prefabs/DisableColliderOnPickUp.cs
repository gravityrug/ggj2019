﻿using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;

public class DisableColliderOnPickUp : MonoBehaviour
{
    HeldObject held;
    Collider col;

    void Start()
    {
        held = this.GetFromHeirarchy<HeldObject>();
        if (!held) return;
        col = this.GetComponentInChildren<Collider>();
        held.PickedUpByCatLady += t => col.enabled = false;
        held.DroppedByCatLady += () => col.enabled = true;
    }

}
