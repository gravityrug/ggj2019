﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillBowl : MonoBehaviour
{
    CatLadyData data;

    private void Start()
    {
        data = this.GetFromHeirarchy<CatLadyData>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (data.HeldObject)
        {
            var CatBowl = data.HeldObject.GetFromHeirarchy<CatBowl>();
            if (CatBowl && !CatBowl.Isfull && other.GetFromHeirarchy<Fridge>()?.GetMilk())
            {
                CatBowl.Fill();
            }
        }
    }
}
