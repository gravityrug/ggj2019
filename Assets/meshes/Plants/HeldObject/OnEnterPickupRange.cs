﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnterPickupRange : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var data = other.GetFromHeirarchy<CatLadyData>();
        if (data)
            data.PossiblePickup = transform.root.gameObject;
    }

    private void OnTriggerExit(Collider other)
    {
        var data = other.GetFromHeirarchy<CatLadyData>();
        if (data?.PossiblePickup == transform.root.gameObject)
            data.PossiblePickup = null;
            
    }
}
