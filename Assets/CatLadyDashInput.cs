using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions
{
	public class CatLadyDashInput : Condition
	{
        CatLadyData data;

        public Inputs.State state;

        private void Start()
        {
            data = this.GetFromHeirarchy<CatLadyData>();
        }

        public override bool Evaluate()
        {
            return data.InputDash == state;
        }
    }
}


