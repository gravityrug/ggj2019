using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions
{
	public class CatLadyMoveInput : Condition
	{
        public Conditional condition = Conditional.GreaterThan;
        [Range(-1f, 1f)]
        public float Input;

        CatLadyData data;

        private void Start()
        {
            data = this.GetFromHeirarchy<CatLadyData>();
        }

        public override bool Evaluate()
        {
            if (condition == Conditional.GreaterThan)
                return data.InputTilt > Input;
            else return data.InputTilt < Input;
        }
    }
}


