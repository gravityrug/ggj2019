using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateBehaviours
{
	public class PlayAudio : StateBehaviour, IEnterState, IUpdateState
	{
        public float delay;
        public AudioAsset sound;
        Transform t;

        void Start()
        {
            t = Camera.main.transform;
        }

        bool playedSound;

        float enterTIme;
		public void EnterState ()
		{
            enterTIme = Time.time; ;
            playedSound = false;
		}

        public void UpdateState()
        {
            if (!playedSound && Time.time - enterTIme > delay)
            {
                playedSound = true;
                AudioSource.PlayClipAtPoint(sound, t.position);
            }
        }
	}
}


