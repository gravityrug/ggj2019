﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPlayer : MonoBehaviour
{

    [SerializeField]
    float _speed = 1;
    ControllerInput _controllerInput;
   

    // Start is called before the first frame update
    void Start()
    {
        _controllerInput = GetComponent<ControllerInput>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //Grab input and move character.
        Vector3 move = new Vector3(_controllerInput.GetAxis(Inputs.Axis.LeftStickX) * _speed,0, _controllerInput.GetAxis(Inputs.Axis.LeftStickY) * _speed);
        transform.position += move;

 

    }
}
