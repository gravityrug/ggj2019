﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatBehaviour : MonoBehaviour
{
    enum CatMetaState { Alive, Die, Reproducing};
    enum CatMovementState { Play, HuntForFood, HuntForFuss, ExitRoom, Pickedup};
    enum Activity { move, cry, drink, fuss };
    bool hasObjective;
    Activity _currentTask;
    Material _material;
    CatMetaState _metaState;
    CatMovementState _movementState;
    Collider _senseSphere;

    public float _currentHunger;
    public float _currentFuss;
    public float _currentHappiness;

    DungeonMaster _DM;

    FindTarget _findTarget;
    Vector3 _targetLocation;
    Transform _targetObject;
    Vector3 _lastLocation;

    [SerializeField]
    float _walkSpeed;

    [SerializeField]
    float _maxPossibleLitterSize = 3.0f;

    [SerializeField]
    float _increaseHappinessStatRate = 1.0f;
    [SerializeField]
    float _decreaseHungerStatRate = 1.0f;
    [SerializeField]
    float _decreaseLastFussStatRate = 1.0f;


    [SerializeField]
    ParticleSystem[] _particles;
    
    // Start is called before the first frame update
    void Start()
    {
        _material = GetComponent<MeshRenderer>().material;
        _findTarget = GetComponentInChildren<FindTarget>();
        _DM = GameObject.FindGameObjectWithTag("DungeonMaster").GetComponent<DungeonMaster>();
        if(_DM == null)
        {
            Debug.LogError("NO Dungeon Master");
        }

        Spawn(transform.position);
    }

    void Spawn(Vector2 spawnPos)
    {
        transform.position = spawnPos;
        _currentFuss = 50.0f;
        _currentHunger = 50.0f;
        _currentHappiness = 0.0f;
        hasObjective = false;
        _currentTask = Activity.cry;
        _movementState = CatMovementState.Play;

    }

    void ColorCheck()
    {
        if (Vector3.Distance(transform.position, _targetLocation) < 0.5f)
        {
            //animate play.
            _material.color = Color.green;
        }
        else
        {
            _material.color = Color.red;
        }
    }

    void CatMood()
    {
      if(_currentHappiness == 100)
        {
            BoxAndReproduce();
        }
       
    }


   
    void  Drink() {

        float milk = _findTarget._milk.GetComponent<milk>().Drink();
        if (milk > 0)
        {
            _currentHunger += milk;
            
        }
        else
        {
            Cry();
        }
    }

    public void Fuss(float fuss) {

        _currentFuss += fuss;

    }



    IEnumerator BoxAndReproduce()
    {
        int litterSize = (int)Random.Range(1, _maxPossibleLitterSize);

        //Animte Box.

        yield return null;

        _DM.SpawnCats(transform.position, litterSize);

        //Despawn this cat.
        _DM.DespawnCat(this);
        
        yield return null;
    }


    void Cry()
    {
        //if (_currentHunger < 40)
        //    _particles[0].Play();
    }


    
    void TickStats()
    {
        _currentHappiness += _increaseHappinessStatRate * Time.deltaTime;
        _currentHunger -= _decreaseHungerStatRate * Time.deltaTime;
        _currentFuss -= _decreaseLastFussStatRate * Time.deltaTime;
       
    }

    void DeathCondition()
    {
        if(_currentHunger <= 0 || _currentFuss <= 0)
        {
            print("CAT DEAD");
        }
    }

  
    private void Update()
    {
        if (_findTarget._milk != null)
        {
            if (Vector3.Distance(transform.position, _findTarget._milk.position) < 0.5f)
            {
                Drink();
            }
            else
            {
                Cry();
            }
        }
        else
        {
            Cry();
        }

        TickStats();
        DeathCondition();

        ColorCheck();


    }
}
