﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class DungeonMaster : MonoBehaviour
{
    [SerializeField]
    CatSystem _CatPrefab = null;

    Laser _Laser;
    List<CatSystem> _cats;
    List<Transform> _milkBowls;

    GameObject[] _catSpawners;
    [SerializeField]
    Vector2 spawnrate;
    [SerializeField]
    float litterPerspawn;
    GameObject multicam;
    int _lives;

    float starttime;
    [SerializeField]
    float GameoverTime;

    [SerializeField]
    Text[] _scores;

    [SerializeField]
    Text _gametime;

    bool GameStarted;
    public List<int> _playerScore;

    // Start is called before the first frame update
    void Start()
    {
        GameStarted = false;
        NewGame();

    }

    public void NewGame()
    {

        _catSpawners = GameObject.FindGameObjectsWithTag("CatSpawner");
        _playerScore = new List<int>();
        _playerScore.Add(0);
        _playerScore.Add(0);
        _playerScore.Add(0);
        _playerScore.Add(0);
        GameStarted = true;
         starttime = Time.time;
        
    }


    float GetGameTime()
    {
        return Time.time - starttime;
    }

    public void SpawnCats(Vector3 spawnPos, int litter)
    {
        //for (int itr = 0; itr < litter; ++itr)
        //{
        //    Vector3 pos = spawnPos;
        //    pos.x += Random.Range(0, 1.0f);
        //    pos.z += Random.Range(0, 1.0f);
        ////    _cats.Add(Instantiate<CatSystem>(_CatPrefab, pos, Quaternion.identity));
        //}
    }


    public void SinglePlayer()
    {
        multicam.SetActive(false);
    }

    void SpawnRandomCat()
    {
        int spawnspot = Random.Range(0, _catSpawners.Length);

        Quaternion q = Random.rotation;
        Vector3 euler = q.eulerAngles;
        euler.x = 0;
        euler.z = 0;

        Instantiate(_CatPrefab, _catSpawners[spawnspot].transform.position, Quaternion.Euler(euler));

    }

    public AudioClip[] playerAudio;

    public void addpoint(int player)
    {
        if (player < 1)
            return;


        //PlayClipAtPoint(playerAudio[player - 1], transform.position);
        if(playerAudio[player-1] != null)
        AudioSource.PlayClipAtPoint(playerAudio[player - 1], transform.position);


        _playerScore[player-1]++;
        _scores[player-1].text = _playerScore[player-1].ToString();

        _scores[player-1].transform.DOPunchScale(new Vector3(2, 2, 2), 0.5f, 5, 1);

    }

    public void DespawnCat(CatBehaviour catB)
    {
        //kill cat and add to tally.
    }

    public void CatDied()
    {
    }


    float _nextSpawnTime;
    // Update is called once per frame
    void Update()
    {
        if (!GameStarted)
            return;

        if (Time.time > _nextSpawnTime)
        {
            for (int itr = 0; itr < litterPerspawn; ++itr)
            {
                SpawnRandomCat();
            }
            _nextSpawnTime += Mathf.Lerp(spawnrate.y, spawnrate.x, (GetGameTime() / 180.0f));

        }

        _gametime.text = (GameoverTime - GetGameTime()).ToString();

        if (GetGameTime() > GameoverTime)
        {
            StartCoroutine("Gameover");
        }



    }

    public GameObject _gameoverPanel;
    public GameObject _scorePanel;
    public Text _winner;
    public GameObject _start;
    public GameObject _timer;

    IEnumerator Gameover()
    {
        _gameoverPanel.SetActive(true);
        _timer.SetActive(false);

        int highscoreplayer;
        highscoreplayer = 1;

        for (int itr = 0; itr < 3; ++itr)
        {
            if (_playerScore[itr + 1] > _playerScore[itr])
            {
                highscoreplayer = itr + 1;
            }

        }

        _winner.text = highscoreplayer.ToString();


        yield return new WaitForSeconds(5.0f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Title");
        yield return null;
    }
}
