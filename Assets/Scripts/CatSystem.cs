﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CatSystem : MonoBehaviour
{

    public float _currentHappiness;
    public float _currentHungerLevel;
    float _currentFussLevel;
    bool isAlive = true;
    bool staystill = false;

    [SerializeField]
    float FussIncreaseRate;
    
    DungeonMaster _dm;
    [SerializeField]
    float FussReductionRate;
    [SerializeField]
    float HungerReductionRate;
    [SerializeField]
    float FoodHappinessRate;
    FindTarget _target;

    [SerializeField]
    ParticleSystem[] _particles;

    [SerializeField]
    float speed;

    public int _lastplayer;

    AudioSource[] _playwin;
    HeldObject _held;
    // Start is called before the first frame update
    void Start()
    {
        //_dm = GameObject.FindGameObjectWithTag("DungeonMaster").GetComponent<DungeonMaster>();
        _lastplayer = -1;
    }

    void Init()
    {

        GetComponent<HeldObject>().PickedUpByCatLady += t =>
        {
            staystill = true;


        };

        GetComponent<HeldObject>().DroppedByCatLady += () =>
        {
            staystill = false;
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 1, 1) * 5.0f);
        };

        _currentFussLevel = 100;
        _currentHungerLevel = 50;
        isAlive = true;
    }
    void Tick()
    {
        // _currentFussLevel -= (Time.deltaTime * FussReductionRate);
        _currentHungerLevel -= (Time.deltaTime * HungerReductionRate);

        //if (_currentHungerLevel <= 0)
        //    StartCoroutine(Dead());
    }

    public void FussOverCat()
    {
        if (_currentFussLevel < 50)
        {
            _currentHappiness += 20;
            _currentFussLevel = 100;
            _particles[0].Play();
        }

    }

    public void FeedMe()
    {
        _currentHungerLevel = 100;
        _currentHappiness += FoodHappinessRate;
        _particles[0].Play();
    }



    IEnumerator Dead()
    {
        isAlive = false;

        transform.rotation = Quaternion.Euler(180, 0, 0);

        yield return new WaitForSeconds(5.0f);


        //if(_dm)
        //    _dm.CatDied();

        //transform.gameObject.SetActive(false);
        //Destroy(gameObject);
        
    }

    void WalkForwardRandomDistance()
    {
       
        
       

        
    }

 
    // Update is called once per frame
    void Update()
    {
        if(isAlive)
        Tick();
        
    }


    public void Pickup(bool isTrue)
    {
        if (isTrue)
        {
            staystill = true;
        }
        else
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(0,1,1) * 5.0f);
        }

        
    }
    [SerializeField]
    AudioClip[] bounceSounds;

    private void OnCollisionStay(Collision collision)
    {
        if(isAlive && !staystill)
         transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isAlive && !staystill)
        {
            Quaternion q = Random.rotation;
            Vector3 euler = q.eulerAngles;
            euler.x = 0;
            euler.z = 0;

            transform.Rotate(new Vector3(0, 1, 0), 45.0f);
        }

        if (collision.collider.CompareTag("wall"))
        {   if (transform.position.y > 0.4f)
            {
                print("bounce");
                AudioSource.PlayClipAtPoint(bounceSounds[Random.Range(0, bounceSounds.Length)], new Vector3(0,5.0f,0));
            }
        }

        if (collision.collider.CompareTag("CatLady"))
        {
            _lastplayer = collision.collider.GetFromHeirarchy<CatLadyData>()._PlayerID;
            
        }
    }
}
