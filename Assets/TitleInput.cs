﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleInput : MonoBehaviour
{
    LoadScene load;
    ControllerInput input;
    private void Start()
    {
        load = GetComponent<LoadScene>();
        input = GetComponent<ControllerInput>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            load.OnLoadScene();
        if (Input.GetKeyDown(KeyCode.Return))
            load.OnLoadScene();
        if (input.GetButton(Inputs.Buttons.Start) == Inputs.State.Pushed)
            load.OnLoadScene();
    }
}
