﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnSpawn : MonoBehaviour
{
    public AudioClip spawn;

    // Start is called before the first frame update
    void Start()
    {
        AudioSource.PlayClipAtPoint(spawn, transform.position);   
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
