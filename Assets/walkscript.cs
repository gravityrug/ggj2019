﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class walkscript : MonoBehaviour
{

    [SerializeField]
    float _speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 nextPos = transform.position;
        nextPos.x = Input.GetAxis("Horizontal") * _speed;
        nextPos.z = Input.GetAxis("Vertical") * _speed;

        nextPos *= Time.deltaTime;

        transform.position = nextPos;
    }
}
