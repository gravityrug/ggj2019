﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnCat : MonoBehaviour
{
    HeldObject HeldObject;
    Transform CamPos;
    public AudioAsset PickUpSounds;
    public AudioAsset DroppedSounds;
    public AudioAsset ThrownSounds;
    public Rigidbody rb;

    public float throwVelocity = 2f;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetFromHeirarchy<Rigidbody>();
        CamPos = Camera.main.transform;
        HeldObject = this.GetFromHeirarchy<HeldObject>();
        HeldObject.PickedUpByCatLady += t =>
        {
            AudioSource.PlayClipAtPoint(PickUpSounds, transform.position);
        };
        HeldObject.DroppedByCatLady += () =>
        {
            if (rb.velocity.magnitude < throwVelocity)
                AudioSource.PlayClipAtPoint(DroppedSounds, transform.position);
            else AudioSource.PlayClipAtPoint(ThrownSounds, CamPos.position, .5f);
        };
    }
}
