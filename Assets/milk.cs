﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class milk : MonoBehaviour
{

    float _milkContent;
    [SerializeField]
    float _milkRate = 5.0f;
    // Start is called before the first frame update
 
        public void Fill()
    {
        _milkContent = 100;
    }



    public float Drink()
    {
        if (_milkContent >= _milkRate)
        {
            _milkContent -= _milkRate;
            return _milkRate;
        }
        else return 0;
    }
}
