﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchMusic : MonoBehaviour
{
    AudioSource source;

    public AudioClip[] songs;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    int currentSong = 0;

    // Update is called once per frame
    void Update()
    {
        if (!source.isPlaying)
        {
            if (currentSong == songs.Length)
            {
                source.clip = songs[songs.Length - 1];
                source.Play();
                return;
            }
            source.clip = songs[currentSong];
            currentSong++;
            source.Play();
        }
    }
}
