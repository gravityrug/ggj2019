﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreasePoints : MonoBehaviour
{
    DungeonMaster _dm;

    // Start is called before the first frame update
    void Start()
    {
        _dm = GameObject.FindGameObjectWithTag("DungeonMaster").GetComponent<DungeonMaster>();    
    }


    private void OnTriggerExit(Collider other)
    {
        if (_dm)
        {
            _dm.addpoint(other.GetFromHeirarchy<CatSystem>()._lastplayer);
            other.gameObject.SetActive(false);
            Destroy(other.gameObject);
        }
    }


}
