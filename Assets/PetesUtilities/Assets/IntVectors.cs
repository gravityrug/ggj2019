﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Int3
{
    public Int3(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Int3(Vector3 vector3)
    {
        this.x = Mathf.FloorToInt(vector3.x);
        this.y = Mathf.FloorToInt(vector3.y);
        this.z = Mathf.FloorToInt(vector3.z);
    }

    public int x;
    public int y;
    public int z;

    /// <summary>
    /// Returns a new Int3 offset by given values
    /// </summary>
    public Int3 Offset(int x, int y, int z)
    {
        return new Int3(this.x + x, this.y + y, this.z + z);
    }

    public override bool Equals(object obj)
    {
        if (!(obj is Int3))
            return false;
        var pos = (Int3)obj;
        return pos.x == x && pos.y == y && pos.z == z;
    }

    public static bool operator ==(Int3 a, Int3 b)
    {
        return a.x == b.x && a.y == b.y && a.z == b.z;
    }

    public static bool operator !=(Int3 a, Int3 b)
    {
        return !(a == b);
    }

    public override int GetHashCode()
    {
        int result;
        result = 6151 * z;
        result += 53 * y;
        result += 1572869 * x;
        return result;
    }

    public override string ToString()
    {
        return string.Format("Int3[{0}, {1}, {2}]", x, y, z);
    }
}

[System.Serializable]
public struct Int2
{
    public Int2(Vector2 vector)
    {
        this.x = Mathf.FloorToInt(vector.x);
        this.y = Mathf.FloorToInt(vector.y);
    }

    public Int2(float x, float y)
    {
        this.x = Mathf.FloorToInt(x);
        this.y = Mathf.FloorToInt(y);
    }

    public Int2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public int x;
    public int y;

    /// <summary>
    /// Returns a new Int2 offset by given values
    /// </summary>
    public Int2 Offset(int x, int y)
    {
        return new Int2(this.x + x, this.y + y);
    }

    public Int2 Offset(float x, float y)
    {
        return new Int2(this.x + x, this.y + y);
    }

    public override bool Equals(object obj)
    {
        if (!(obj is Int2))
            return false;
        var pos = (Int2)obj;
        return pos.x == x && pos.y == y;
    }

    public static bool operator ==(Int2 a, Int2 b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static Int2 operator *(Int2 a, int b)
    {
        return new Int2(a.x * b, a.y * b);
    }

    public static bool operator !=(Int2 a, Int2 b)
    {
        return !(a==b);
    }

    public override int GetHashCode()
    {
        int result;
        result = 53 * x;
        result += 1572869 * y;
        return result;
    }

    public override string ToString()
    {
        return string.Format("IntVec2[{0}x:{1}y]", x, y);
    }
}