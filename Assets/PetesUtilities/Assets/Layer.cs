﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Assets/Layer")]
public class Layer : ScriptableObject 
{
	[SerializeField]
	LayerMask layer = new LayerMask() { value = 1};

	public static implicit operator LayerMask(Layer obj)
	{
		return obj.layer;
	}

	public static implicit operator int(Layer obj)
	{
		return obj.layer.value;
	}
}
