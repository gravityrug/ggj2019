﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Assets/AnimatorEvent")]
public class AnimatorEvent : ScriptableObject
{
}
