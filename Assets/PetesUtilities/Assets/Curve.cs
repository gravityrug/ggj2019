﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
    [CreateAssetMenu(menuName = "Assets/Curve")]
    public class Curve : ScriptableObject
    {
        [HideInInspector]
        public List<SerializableKeyframe> keys;

        [System.NonSerialized]
        public AnimationCurve curve;
        
        private void OnEnable()
        {
            curve = new AnimationCurve();
            curve.preWrapMode = WrapMode.ClampForever;
            curve.postWrapMode = WrapMode.ClampForever;
            foreach (var key in keys)
                curve.AddKey(key.ToKeyFrame());
        }

        public static implicit operator AnimationCurve(Curve obj)
        {
            return obj.curve;
        }

        [System.Serializable]
        public class SerializableKeyframe
        {
            public SerializableKeyframe(Keyframe key)
            {
                time = key.time;
                value = key.value;
                inTangent = key.inTangent;
                outTangent = key.outTangent;
                inWeight = key.inWeight;
                outWeight = key.outWeight;                
            }

            public float time;
            public float value;
            public float inTangent;
            public float outTangent;
            public float inWeight;
            public float outWeight;

            public Keyframe ToKeyFrame()
            {
                return new Keyframe(time, value, inTangent, outTangent, inWeight, outWeight);
            }
        }
    }
}

[System.Serializable]
public class CurveReference
{
    public AnimationCurve Value
    {
        get { return useRef ? refCurve : curve;}
    }

    [SerializeField]
    bool useRef = true;
    [SerializeField]
    Assets.Curve refCurve = null;
    [SerializeField]
    AnimationCurve curve = null;
    
    public static implicit operator AnimationCurve(CurveReference obj)
    {
        return obj.Value;
    }
}
#if UNITY_EDITOR
namespace Inspectors
{
    using UnityEditor;

    [CustomEditor(typeof(Assets.Curve))]
    class CurveInspector : Editor
    {
        Assets.Curve asset;

        private void OnEnable()
        {
            asset = (Assets.Curve)target;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script");

            asset.curve = EditorGUILayout.CurveField(asset.curve);
            serializedObject.ApplyModifiedProperties();
        }

        private void OnDisable()
        {
            EditorUtility.SetDirty(target);

            asset.keys.Clear();
            foreach (var key in asset.curve.keys)
                asset.keys.Add(new Assets.Curve.SerializableKeyframe(key));
        }
    }

    [CustomPropertyDrawer(typeof(CurveReference))]
    public class CurveReferencePropertyDrawer : PropertyDrawer
    {
        string[] names = { "Value", "Reference" };

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var useRef = property.FindPropertyRelative("useRef");
            var rect = position;
            rect.height = EditorGUIUtility.singleLineHeight;

            int selection = useRef.boolValue ? 1 : 0;

            var width = rect.width / 5f;
            rect.width = width;
            GUIContent display = new GUIContent(property.displayName, "Use a referenced Curve or use a Value");
            EditorGUI.LabelField(rect, display);
            rect.x += width;
            selection = EditorGUI.Popup(rect, selection, names);
            rect.x += width;
            useRef.boolValue = selection == 1 ? true : false;

            rect.width += width * 2f;
            if (useRef.boolValue)
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("refCurve"), GUIContent.none);
            else
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("curve"), GUIContent.none);
            //EditorGUI.Popup()

            //EditorGUI.Popup()
        }
    }
}

#endif
