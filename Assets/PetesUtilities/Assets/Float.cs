﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
    [CreateAssetMenu(menuName = "Assets/Float")]
    public class Float : ScriptableObject
    {
        public float Value;
        public static implicit operator float(Float obj)
        {
            return obj.Value;
        }
    }
}

[System.Serializable]
public class FloatReference
{
    [SerializeField] 
    public bool useRef = false;
    [SerializeField]
    Assets.Float reference = null;
    [SerializeField]
    float instance = 0f;

    public FloatReference(float value)
    {
        instance = value;
    }

    public float Value
    {
        get { return useRef ? reference : instance; }
    }

    public static implicit operator float(FloatReference obj)
    {
        return obj.Value;
    }
}
#if UNITY_EDITOR
namespace Inspectors
{
    using UnityEditor;

    [CustomPropertyDrawer(typeof(FloatReference))]
    public class FloatReferencePropertyDrawer : PropertyDrawer
    {
        string[] names = { "Value", "Reference" };

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var useRef = property.FindPropertyRelative("useRef");
            var rect = position;
            rect.height = EditorGUIUtility.singleLineHeight;

            int selection = useRef.boolValue ? 1 : 0;

            var width = rect.width / 5f;
            rect.width = width;
            GUIContent display = new GUIContent(property.displayName, "Use a referenced Float or use a Value");
            EditorGUI.LabelField(rect, display);
            rect.x += width;
            selection = EditorGUI.Popup(rect, selection, names);
            rect.x += width;
            useRef.boolValue = selection == 1 ? true : false;

            rect.width += width * 2f;
            if (useRef.boolValue)
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("reference"), GUIContent.none);
            else
                EditorGUI.PropertyField(rect, property.FindPropertyRelative("instance"), GUIContent.none);
        }
    }
}

#endif
