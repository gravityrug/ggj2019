﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Assets/Audio")]
public class AudioAsset : ScriptableObject
{
    public List<AudioClip> clips = new List<AudioClip>();

    public static implicit operator AudioClip(AudioAsset obj)
    {
        if (obj.clips.Count > 0)
        {
            var clip = obj.clips[Random.Range(0, obj.clips.Count)];
#if UNITY_EDITOR
            if (!clip)
                Debug.LogError("Audio Asset {0} returned a null clip");
#endif
            return clip;
        }

        Debug.Log(string.Format("Audio Asset {0} missing clips", obj.name), obj);
        return null;
    }
}
