﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadScene : MonoBehaviour {
    public SceneField scene;

    public KeyCode LoadOnPress;

    public void OnLoadScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    }

    private void Update()
    {
        if (Input.GetKeyDown(LoadOnPress))
        {
            OnLoadScene();
        }
    }
}
