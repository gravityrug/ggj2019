﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Assets/Texture Array Generator")]
public class TextureArrayGenerator : ScriptableObject
{
	public bool useMipMaps = true;
	public TextureFormat format = TextureFormat.ARGB32;
	public Texture2DArray textureArray;
	public List<Texture2D> textures = new List<Texture2D>(){null};
	public string textureName = "NewTextureArray";
	SerializedObject so;
	public int Dimensions = 512;
	public int scale;
	public string path;
	public TextureWrapMode wrapmode = TextureWrapMode.Repeat;
	public int anisoLevel = 1;
	public bool linear;

	GUIStyle hStyle;
	GUIStyle vStyle;
}

#if UNITY_EDITOR
[CustomEditor(typeof(TextureArrayGenerator))]
class TextureArrayGeneratorInspector : Editor
{

	Vector2 position;

	void OnEnable()
	{
		
	}

	public override void OnInspectorGUI ()
	{
		var TGen = (TextureArrayGenerator)target;

		var hStyle = GUI.skin.horizontalScrollbar;
		var vStyle = GUI.skin.verticalScrollbar;
		string notValidInfo = "";
		bool valid = true;

		var textures = TGen.textures;
		var Dimensions = TGen.Dimensions;
		var so = serializedObject;
		var textureName = TGen.textureName;
		var textureArray = TGen.textureArray;
		var anisoLevel = TGen.anisoLevel;
		var scale = TGen.scale;
		var path = TGen.path;

		if (textures.Count == 0 || textures[0] == null)
		{
			notValidInfo = "Need texture in fist slot";
			valid = false;
		}
		else 
		{
			if (Dimensions <= 0)
			{
				notValidInfo = "Dimensions Need to be greater than 0";
				valid = false;
			}

			else
			{
				foreach (var item in textures)
				{
					if (item == null)
					{
						notValidInfo = "Texture Field Cannot be Null";
						valid = false;
						break;
					}
				}	
			}

			if (string.IsNullOrEmpty(textureName) && !textureArray)
			{
				notValidInfo = "Texture Asset Needs a Name";
				valid = false;
			}

			if (textures.Count > 256)
			{
				notValidInfo = "Max Textures exceeded, Maximum 256 Textures";
				valid = false;
			}
		}

		so.Update();

		EditorGUI.BeginChangeCheck();

		EditorGUILayout.PropertyField(so.FindProperty("textureArray"));
		if (textureArray)
			GUI.enabled = false;
		EditorGUILayout.PropertyField(so.FindProperty("textureName"));
		EditorGUILayout.PropertyField(so.FindProperty("path"));
		GUI.enabled = true;

		EditorGUILayout.PropertyField(so.FindProperty("Dimensions"));
		Dimensions = Mathf.ClosestPowerOfTwo(Dimensions);

		EditorGUILayout.PropertyField(so.FindProperty("useMipMaps"));
		EditorGUILayout.PropertyField(so.FindProperty("linear"));

		anisoLevel = EditorGUILayout.IntSlider("Aniso Level", anisoLevel, 0, 16);

		EditorGUILayout.PropertyField(so.FindProperty("wrapmode"));
		EditorGUILayout.PropertyField(so.FindProperty("format"));

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PropertyField(so.FindProperty("textures"), new GUIContent(string.Format("{0} Textures (Drop Textures Here)", textures.Count)));

		TGen.scale = EditorGUILayout.IntSlider(GUIContent.none, scale, 64, 128, GUILayout.MaxWidth(128f)); 

		if (GUILayout.Button("Clear All"))
		{
			TGen.textures.Clear();
			TGen.textures.Add(null);
		}

		EditorGUILayout.EndHorizontal();

		so.ApplyModifiedProperties();

		var tex = so.FindProperty("textures");

		so.Update();

		EditorGUI.BeginChangeCheck();


		GUI.skin.horizontalScrollbar = GUIStyle.none;

		int addElement = -1;
		int removeElement = -1;

		if (tex.isExpanded && tex.arraySize > 0)
		{
			var height =  8f + (float)Mathf.Floor(tex.arraySize / (Screen.width/(scale + 18)))* (scale+4);

			position = EditorGUILayout.BeginScrollView(position, false, false);

			EditorGUILayout.LabelField("", GUIStyle.none, GUILayout.Height(height + scale + 4));

			for (int i = 0; i < tex.arraySize; ++i)
			{
				var item = tex.GetArrayElementAtIndex(i);

				var rect = new Rect();
				rect.x = (scale + 18) * (i % Mathf.Floor(Screen.width / (scale+18))) + 8;
				rect.y = (scale + 4) * Mathf.Floor(i/(Mathf.Floor(Screen.width / (scale+18))));
				rect.width = scale;
				rect.height = scale;

				item.objectReferenceValue = EditorGUI.ObjectField(rect, item.objectReferenceValue, typeof(Texture2D), false);

				rect.width = 16;
				rect.height = 16;
				rect.x += scale + 1;

				if (GUI.Button( rect, "+"))
				{
					addElement = i;
				}

				rect.y += 18;

				if (GUI.Button( rect, "-"))
				{
					removeElement = i;
				}
			}
			EditorGUILayout.EndScrollView();

		}
		else
		{
			position = EditorGUILayout.BeginScrollView(position, false, false);
			for (int i = 0; i < tex.arraySize; ++i)
			{
				EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(Screen.width - 32));
				EditorGUILayout.PropertyField(tex.GetArrayElementAtIndex(i), GUIContent.none);

				if (GUILayout.Button("+", GUILayout.MaxWidth(16)))
				{
					addElement = i;
				}

				if (GUILayout.Button("-", GUILayout.MaxWidth(16)))
				{
					removeElement = i;
				}

				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.EndScrollView();
		}

		if (textures.Count == 0)
		{
			if (GUILayout.Button("Add Texture"))
			{
				textures.Add(null);
			}
		}

		if (addElement >= 0)
		{
			tex.InsertArrayElementAtIndex(addElement);
			tex.GetArrayElementAtIndex(addElement+1).objectReferenceValue = null;
		}
		if (removeElement >= 0)
		{
			tex.DeleteArrayElementAtIndex(removeElement);
		}

		so.ApplyModifiedProperties();

		GUI.skin.horizontalScrollbar = hStyle;
		GUI.skin.verticalScrollbar = vStyle;

		if (EditorGUI.EndChangeCheck())
			return;

		if (!valid)
		{
			EditorGUILayout.HelpBox(notValidInfo, MessageType.Error);
			GUI.enabled = false;
		}	
		else if (!AssetDatabase.IsValidFolder(path) && TGen.textureArray == null)
		{
			EditorGUILayout.HelpBox("Path not Valid, placing texture under Assets Folder", MessageType.Warning);
		}	

		string createName;

		if (textureArray != null)
			createName = "Modify Texture Asset";
		else 
			createName = "Create Texture Asset";

		if (GUILayout.Button(createName))
		{
			textureArray = new Texture2DArray(Dimensions, Dimensions, textures.Count, TGen.format, TGen.useMipMaps, TGen.linear);
			textureArray.anisoLevel = TGen.anisoLevel;
			textureArray.wrapMode = TGen.wrapmode;
			RenderTexture cache = RenderTexture.active;
			bool srbWrite = GL.sRGBWrite;
			GL.sRGBWrite = !TGen.linear;
			for (int i = 0; i < textures.Count; ++ i)
			{
				var refTex = textures[i];

				var render = RenderTexture.GetTemporary(Dimensions, Dimensions, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);

				Graphics.Blit(refTex, render);

				RenderTexture.active = render;

				Texture2D sourceTex = new Texture2D(Dimensions, Dimensions, TextureFormat.ARGB32, false, false);
				sourceTex.ReadPixels(new Rect(0,0, Dimensions, Dimensions), 0,0, false);
				sourceTex.Apply();

				RenderTexture.ReleaseTemporary(render);

				//if (sourceTex.width != textureArray.width || sourceTex.height != textureArray.height)
				//	sourceTex.Resize(Width, Height);

				textureArray.SetPixels(sourceTex.GetPixels(), i, 0);
			}
			textureArray.Apply(TGen.useMipMaps);
			RenderTexture.active = cache;
			GL.sRGBWrite = srbWrite;

			if (TGen.textureArray)
			{
				EditorUtility.CopySerialized(textureArray, TGen.textureArray);
				AssetDatabase.SaveAssets();
			}
			else
			{
				if (!AssetDatabase.IsValidFolder(path))
					path = "Assets/";
				AssetDatabase.CreateAsset(textureArray, path + "/" + textureName + ".asset");		
			}		
			EditorGUIUtility.PingObject(textureArray);
		}
	}
}

#endif