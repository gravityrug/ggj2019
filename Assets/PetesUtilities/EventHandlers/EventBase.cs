﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBase : MonoBehaviour
{
    public Dictionary<int, object> Events = new Dictionary<int, object>();

    public void Subscribe<T>(IEvent<T> subscriber)
    {
        object val;
        if (Events.TryGetValue(TypeID<T>.ID, out val))
        {
            EventHolder<T> holder = val as EventHolder<T>;
            holder.Add(subscriber);
        }
        else
        {
            EventHolder<T> holder = new EventHolder<T>();
            Events[TypeID<T>.ID] = holder;
            holder.Add(subscriber);
        }
    }

    public void UnSubscribe<T>(IEvent<T> subscriber)
    {
        object val;
        if (Events.TryGetValue(TypeID<T>.ID, out val))
        {
            EventHolder<T> holder = val as EventHolder<T>;
            holder.Remove(subscriber);
        }
    }

    public void Invoke<T>(Component invoker, T args)
    {
        object val;
        if (Events.TryGetValue(TypeID<T>.ID, out val))
        {
            EventHolder<T> holder = val as EventHolder<T>;
            holder.Invoke(invoker, args);
        }
    }
}

public abstract class EventHolder
{
#if UNITY_EDITOR
    public Queue<Component> Invokers = new Queue<Component>();
    public int invokes = 0;

    public virtual List<Component> GetSubscribers()
    {
        return null;
    }

    public virtual int SubscriberCount()
    {
        return 0;
    }
#endif
}

public class EventHolder<T> : EventHolder
{
    public int lastPosition = -1;
    public IEvent<T>[] Events = new IEvent<T>[4];

    public void Add(IEvent<T> item)
    {
        lastPosition++;
        if (lastPosition == Events.Length)
            System.Array.Resize(ref Events, Events.Length * 2);
        Events[lastPosition] = item;
    }

    public void Remove(IEvent<T> item)
    {
        for (int i = lastPosition; i >= 0; --i)
        {
            if (item == Events[i])
            {
                Events[i] = null;
                Events[i] = Events[lastPosition];
                lastPosition--;
                break;
            }
        }
    }

    public void Invoke(Component invoker, T args)
    {
#if UNITY_EDITOR
        Invokers.Enqueue(invoker);
        if (Invokers.Count > 50)
            Invokers.Dequeue();
        invokes++;
#endif

        for (int i = lastPosition; i >= 0; --i)
        {
            var e = Events[i];
            if (e == null)
            {
                Events[i] = Events[lastPosition];
                lastPosition--;
            }
            else e.OnEvent(args);    
        }
    }
#if UNITY_EDITOR
    public override List<Component> GetSubscribers()
    {
        var subscribers = new List<Component>();
        foreach (var item in Events)
            subscribers.Add(item as Component);
        return subscribers;
    }

    public override int SubscriberCount()
    {
        return lastPosition + 1;
    }
    public override string ToString()
    {
        return typeof(T).Name;
    }
#endif
}

public static class EventExtensions
{
    static Dictionary<int, EventBase> Handlers = new Dictionary<int, EventBase>(); //used to cache Event bases to make the search a bit faster

    /// <summary>
    /// Starts listening for any events sent to the nearest parent Event Base
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="co"></param>
    /// <returns></returns>
    public static Component StartEventCallBacks<T>(this Component co)
    {
        IEvent<T> e = co as IEvent<T>;
        if (e != null)
        {
            EventBase ge;
            if (Handlers.TryGetValue(co.gameObject.GetInstanceID(), out ge))
            {
                if (ge)
                {
                    ge.Subscribe(e);
                    return co;
                }
            }

            var item = co.GetComponentsInParent<EventBase>(true);
            if (item.Length > 0)
            {
                item[0].Subscribe(co as IEvent<T>);
                Handlers[co.gameObject.GetInstanceID()] = item[0];
            }
            else
            {
                Debug.LogErrorFormat(co, "Event<{0}> on Behaviour {1}({2}) Cannot Start Listener since no GameEventHandler was found in Parent Heirarchy", typeof(T).Name, co?.GetType().Name, co?.gameObject.name);
            }
        }
        else
            Debug.LogErrorFormat(co, "Cannot start listener IEvent<{2}> on {0}({1}) as component is either null or does not implement interface", co?.GetType().Name, co?.gameObject.name, typeof(T).Name);
        
        return co;
    }

    /// <summary>
    /// Stops listening for any Events sent to the nearest parent Event Base
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="co"></param>
    /// <returns></returns>
    public static Component StopEventCallbacks<T>(this Component co)
    {
        EventBase ge;
        if (Handlers.TryGetValue(co.gameObject.GetInstanceID(), out ge))
        {
            ge?.UnSubscribe(co as IEvent<T>);
        }
        return co;
    }

    /// <summary>
    /// Sends an Event to the nearest parent Event Base found on the target
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="co"></param>
    /// <param name="target"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Component SendEvent<T>(this Component co, Component target, T args)
    {
        EventBase ge;
        if (Handlers.TryGetValue(target.gameObject.GetInstanceID(), out ge))
        {
            if (ge)
            {
                ge.Invoke(co, args);
                return co;
            }
        }
        
        var ges = target.GetComponentsInParent<EventBase>(true);
        if (ges.Length > 0)
        {
            ge = ges[0];
            ge.Invoke(co, args);
            Handlers[target.gameObject.GetInstanceID()] = ge;
        }
        return co;
    }

    /// <summary>
    /// Starts listening for any events sent to the Global Event Manager.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    /// <returns></returns>
    public static Component StartGlobalEventCallbacks<T>(this Component component)
    {
        Global.Subscribe(component as IEvent<T>);
        return component;
    }

    /// <summary>
    /// Stops listening for any events sent to the Global Event Manager
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    /// <returns></returns>
    public static Component StopGlobalEventCallbacks<T>(this Component component)
    {
        Global.UnSubscribe(component as IEvent<T>);
        return component;
    }

    /// <summary>
    /// Sends an Event to the Global Event Manager
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    public static Component SendGlobalEvent<T>(this Component component, T args)
    {
        Global.Invoke(component, args);
        return component;
    }
    
    static bool loaded;
    static EventBase global = null;
    static EventBase Global
    {
        get
        {
            if (!loaded && !global)
            {
                loaded = true;
                var go = new GameObject("Global Event Manager");
                global = go.AddComponent<EventBase>();
                GameObject.DontDestroyOnLoad(go);
            }
            return global;
        }
    }
}

/// <summary>
/// Implements a callback method for the GameEventHandler
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IEvent<T>
{
    void OnEvent(T args);
}

#if UNITY_EDITOR
namespace Inspectors
{
    using System;
    using UnityEditor;

    [CustomEditor(typeof(EventBase))]
    class EventHandlerInspector : Editor
    {
        static string[] selectionOptions = new string[] { "Subscribers", "Invokers"};
        static int selection = 0;
        static string search = "";
        static Dictionary<Type, bool> showFields = new Dictionary<Type, bool>();
        
        public override void OnInspectorGUI()
        {
            EventBase handler = target as EventBase;

            selection = GUILayout.Toolbar(selection, selectionOptions);

            var style = new GUIStyle(EditorStyles.helpBox);
            style.fontSize = 10;
            style.fontStyle = FontStyle.Bold;

            search = EditorGUILayout.TextField("Search Event", search);

            search = search.Replace(" ", "");

            List<object> fields  = new List<object>();

            foreach (var pair in handler.Events)
            {
                if (pair.Value.ToString().IndexOf(search, StringComparison.OrdinalIgnoreCase) >= 0)
                    fields.Add(pair.Value);
            }

            fields.Sort((x, y) => x.ToString().CompareTo(y.ToString()));

            if (selection == 0)
            {
                foreach (var item in fields)
                {
                    var subscribers = (item as EventHolder).GetSubscribers();
                    bool show = false;
                    showFields.TryGetValue(item.GetType(), out show);

                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    {
                        EditorGUI.indentLevel++;
                        showFields[item.GetType()] = EditorGUILayout.Foldout(show, string.Format("{0} ({1} listeners)", item.ToString(), (item as EventHolder).SubscriberCount()));
                        EditorGUI.indentLevel--;
                        if (show)
                        {
                            foreach (var subscriber in subscribers)
                            {
                                if (subscriber == null)
                                    continue;
                                EditorGUILayout.BeginHorizontal();
                                EditorGUILayout.LabelField("", GUILayout.Width(24f));
                                if (GUILayout.Button(string.Format("{0} ({1})", subscriber.GetType().Name, subscriber.gameObject.name), GUI.skin.label))
                                {
                                    EditorGUIUtility.PingObject(subscriber);
                                }
                                EditorGUILayout.EndHorizontal();
                            }
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
            }
            else
            {
                foreach (var item in fields)
                {
                    var invokers = (item as EventHolder).Invokers;
                    bool show = false;
                    showFields.TryGetValue(item.GetType(), out show);

                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    {
                        EditorGUI.indentLevel++;
                        showFields[item.GetType()] = EditorGUILayout.Foldout(show, string.Format("{0} ({1} invokes)", item.ToString(), (item as EventHolder).invokes));
                        EditorGUI.indentLevel--;
                        if (show)
                        {
                            List<Component> list = new List<Component>();
                            foreach (var invoker in invokers)
                            {
                                list.Add(invoker);

                            }
                            list.Reverse();

                            foreach (var invoker in list)
                            {
                                EditorGUILayout.BeginHorizontal();
                                EditorGUILayout.LabelField("", GUILayout.Width(24f));
                                if (invoker == null)
                                    EditorGUILayout.LabelField("Invoker Deleted");
                                else if (GUILayout.Button(string.Format("{0} ({1})", invoker.GetType().Name, invoker.gameObject.name), GUI.skin.label))
                                {
                                    EditorGUIUtility.PingObject(invoker);
                                }
                                EditorGUILayout.EndHorizontal();

                            }
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
            }

        }

        public override bool RequiresConstantRepaint()
        {
            return true;
        }

    }

}


#endif