﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEventRelay : MonoBehaviour
{
    void NewEvent(Object obj)
    {
        var e = obj as AnimatorEvent;
        if (e)
            this.SendEvent(this, e);
    }
}
