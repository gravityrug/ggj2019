﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;

public class PerformanceTest
{
	static Stopwatch watch = new Stopwatch();

	public static void Start(string TestName, int iterations, Action Test)
	{
		watch.Reset();
		watch.Start();
		for (int i = 0; i < iterations; ++i)
		{
			Test();
		}
		watch.Stop();
        Debug.LogFormat("{0}:   average - {1:0.##}ms    total - {2:0.##}ms    iterations {3}", TestName, watch.ElapsedMilliseconds / iterations, watch.ElapsedMilliseconds, iterations);
	}

    public static void Start(string TestName, int iterations, Action Test, MessageType messageType)
    {
        watch.Reset();
        watch.Start();
        for (int i = 0; i < iterations; ++i)
        {
            Test();
        }
        watch.Stop();
        switch (messageType)
        {
            case MessageType.Milliseconds:
                Debug.LogFormat("{0}:   average: {1:0.##}ms     total: {2:0.##}ms   iterations: {3}", TestName, watch.ElapsedMilliseconds / (long)iterations, watch.ElapsedMilliseconds, iterations);
                return;
            case MessageType.Ticks:
                Debug.LogFormat("{0}:   average: {1}ticks       total: {2}ticks     iterations: {3}", TestName, watch.ElapsedTicks / (long)iterations, watch.ElapsedTicks, iterations);
                return;
        }
    }

    public enum MessageType
    {
        Ticks,
        Milliseconds
    }
}
