﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnumUtility
{
    static class EnumLookup<T> where T : System.Enum
    {
        static EnumLookup()
        {
            valueLookup = new Dictionary<string, T>();
            stringLookup = new Dictionary<T, string>();

            names = System.Enum.GetNames(typeof(T));
            values = (T[])System.Enum.GetValues(typeof(T));

            for (int i = 0; i < names.Length; ++i)
            {
                valueLookup.Add(names[i], values[i]);
                stringLookup.Add(values[i], names[i]);
            }
        }

        public static Dictionary<T, string> stringLookup;
        public static Dictionary<string, T> valueLookup;
        public static T[] values;
        public static string[] names;
    }

    /// <summary>
    /// returns all values in <typeparamref name="T"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T[] GetValues<T>() where T: System.Enum
    {
        return EnumLookup<T>.values;
    }

    /// <summary>
    /// Returns all names in <typeparamref name="T"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static string[] GetNames<T>() where T : System.Enum
    {
        return EnumLookup<T>.names;
    }

    /// <summary>
    /// converts a string to matching <typeparamref name="T"/> value, if no match occurs returns default <typeparamref name="T"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this string value) where T : System.Enum
    {
        T lookup;
        if (EnumLookup<T>.valueLookup.TryGetValue(value, out lookup))
            return lookup;
        return default(T);
    }

    /// <summary>
    /// converts a string to matching <typeparamref name="T"/> value, if no match occurs returns default <typeparamref name="T"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this string value, out bool containsValue) where T : System.Enum
    {
        T lookup = default(T);
        containsValue = EnumLookup<T>.valueLookup.TryGetValue(value, out lookup);
        return lookup;
    }



    /// <summary>
    /// Returns the name of the enum as a string
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string GetName<T>(this T value) where T : System.Enum
    {
        return EnumLookup<T>.stringLookup[value];
    }
}
