﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameObjectPool : Manager<GameObjectPool>
{
    class Pool
    {
        public GameObject Reference;
        public Stack<PoolableObject> StoredInstances = new Stack<PoolableObject>();
    }

    Dictionary<int, Pool> pools = new Dictionary<int, Pool>();

    Scene ObjectPoolScene;

    private void Awake()
    {
        ObjectPoolScene = SceneManager.CreateScene("Object Pool");
    }

    static bool pool;
    static bool canPool
    {
        get {
            if (quit || !pool)
                return false;
            return pool;
        }
        set { pool = value; }
    }
    private void OnEnable()
    {
        canPool = true;
#if UNITY_EDITOR
        if (!ObjectPoolScene.isLoaded)
            SceneManager.LoadScene(ObjectPoolScene.name, LoadSceneMode.Additive);
#endif
    }

    private void OnDisable()
    {
        canPool = false;
    }

    public GameObject GetGameObject(GameObject Original)
    {
        GameObject newObject;
        Pool pool;
        if (pools.TryGetValue(Original.GetInstanceID(), out pool))
        {
            while (pool.StoredInstances.Count > 0)
            {
                var go = pool.StoredInstances.Pop();
                if (go)
                {
                    go.pooled = false;
                    return go.gameObject;
                }
            }
        }
        else
        {
            pool = new Pool();
            pools.Add(Original.GetInstanceID(), pool);
            pool.Reference = Instantiate(Original);
            pool.Reference.name = pool.Reference.name.Replace("Clone", "");
            pool.Reference.AddComponent<PoolableObject>().PoolID = Original.GetInstanceID();
            pool.Reference.gameObject.SetActive(false);
            SceneManager.MoveGameObjectToScene(pool.Reference, ObjectPoolScene);
            pools.Add(Original.GetInstanceID(), pool);
        }
        newObject = Instantiate(pool.Reference);
        return newObject;
    }

    public static void SendToPool(PoolableObject poolableObject)
    {
        if (poolableObject.pooled)
            return;

        if (!canPool)
        {
            Destroy(poolableObject.gameObject);
            return;
        }
        poolableObject.gameObject.SetActive(false);
        poolableObject.pooled = true;

        Pool pool;
        if (Get.pools.TryGetValue(poolableObject.PoolID, out pool))
        {
            pool.StoredInstances.Push(poolableObject);
        }
        else
        {
            Debug.LogErrorFormat("{0} pool has not been Initialized", poolableObject.name.Replace("(Clone)", ""));
        }
    }
}

public class PoolableObject : MonoBehaviour
{
    public bool pooled;
    public int PoolID;
}

public static class GameObjectPoolExtensions
{
    /// <summary>
    /// Returns an inactive copy of the gameobject from pool
    /// </summary>
    public static GameObject GetFromPool(this GameObject go)
    {
        if (!go)
            return null;
        return GameObjectPool.Get.GetGameObject(go);
    }

    /// <summary>
    /// Puts the gameobject back into the pool.
    /// </summary>
    /// <param name="go"></param>
    public static void PoolObject(this GameObject go)
    {
        if (!go)
            return;

        var poolable = go.GetComponent<PoolableObject>();

        if (!poolable)
        {
            Debug.LogErrorFormat(go, "Cannot pool objects not made by pool {0}. Destroying Object instead", go.name);
            GameObject.Destroy(go);
        }
        else
        {
            GameObjectPool.SendToPool(poolable);
        }        
    }
}
