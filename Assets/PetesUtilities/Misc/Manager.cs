﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Manager : MonoBehaviour
{
    protected static bool quit;   

    private void OnApplicationQuit()
    {
        quit = true;
    }

}

public abstract class Manager<T> : Manager where T: Manager<T>
{
    static T instance;
    /// <summary>
    /// Returns the instance of the manager
    /// </summary>
    public static T Get
    {
        get
        {
            if (!instance)
                InitializeManager();
            return instance;
        }
    }

    static void InitializeManager()
    {
        if (Application.isPlaying && !quit && !instance)
        {   
            instance = new GameObject(typeof(T).Name).AddComponent<T>();
        }
    }
}
