﻿/// <summary>
/// static instance of type I can use as a buffer to avoid gc
/// </summary>
public static class Buffer<T>
{
    public static T buffer = default(T);
}
