﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Generates Psuedo Random Numbers based on a seed value
/// </summary>
public class RandomGen
{
    static RandomGen instance = new RandomGen();
    public static RandomGen Instance
    {
        get { return instance; }
    }

    public RandomGen()
    {
        Initialize((int)System.DateTime.Now.Ticks);
    }

    public RandomGen(int seed)
    {
        Initialize(seed);
    }

    static StringBuilder builder = new StringBuilder();
    static string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    /// <summary>
    /// Generates a random string using letters provided.
    /// </summary>
    public string String(string LettersToUse, int Length)
    {
        builder.Length = 0;
        builder.EnsureCapacity(Length);
        var useLength = LettersToUse.Length;
        for (int i = 0; i < Length; ++i)
        {
            builder.Append(LettersToUse[Range(useLength - 1)]);
        }
        return builder.ToString();
    }

    /// <summary>
    /// Generates a random string of letters and numbers.
    /// </summary>
    public string String(int Length)
    {
        return String(letters, Length);
    }

    /// <summary>
    /// Re initializes RandomGen to use seed value.
    /// </summary>
    public RandomGen Initialize(int seed)
    {
        x = 1610612741;
        y = seed;
        Seed = seed;
        return this;
    }

    /// <summary>
    /// Current Seed used.
    /// </summary>
    public int Seed
    {
        get;
        private set;
    }

    int x, y;

    /// <summary>
    /// Generates a Pseudo Random Int based on seed
    /// </summary>
    public int Int
    {
        get
        {
            int t = (x ^ x << 11);
            x = y;
            return (y = (y^(y>>16))^(t^(t>>8)));
        }
    }

    static float intMax = (float)int.MaxValue;

    /// <summary>
    /// Generates a Pseudo Random Float between 0 and 1
    /// </summary>
    public float Float
    {
        get
        {
            return (float)Int / intMax;
        }
    }

    public float Range(float Max)
    {
        return Float * Max;
    }

    public float Range(float Min, float Max)
    {
        return Min < Max ? Min + Range(Max - Min) : Max + Range(Min - Max);
    }

    /// <summary>
    /// Returns a number between 0 and Max.
    /// Max number Excluded
    /// </summary>
    public int Range(int Max)
    {
        int t = (x ^ x << 11);
        x = y;
        y = (y ^ (y >> 16)) ^ (t ^ (t >> 8));
        return Max < 0 ? -y%Max : y%Max;
    }

    /// <summary>
    /// Returns a number between Min and Max.
    /// Max number Excluded
    /// </summary>
    public int Range(int Min, int Max)
    {
        int t = (x ^ x << 11);
        x = y;
        y = (y ^ (y >> 16)) ^ (t ^ (t >> 8));
        return Min > Max ?
            Max + y%(Max-Min) :
            Min + y%(Max-Min) ; 
    }

    public List<T> RandomizeList<T>(ref List<T> list)
    {
        for (int i = list.Count-1; i >= 1; --i)
        {
            var index = Range(i);
            var item = list[index];
            list[index] = list[i];
            list[i] = item;
        }
        return list;
    }
}
