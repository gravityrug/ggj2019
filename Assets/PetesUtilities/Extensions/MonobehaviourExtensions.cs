﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MonobehaviourExtensions
{
	public static T GetOrAddComponent<T>(this Component component) where T: Component
	{
		T c;
		c = component.GetComponent<T>();
		if (!c)
			c = component.gameObject.AddComponent<T>();
		return c;
	}

	public static T GetOrAddComponent<T>(this GameObject gameObject) where T: Component
	{
		T c;
		c = gameObject.GetComponent<T>();
		if (!c)
			c = gameObject.gameObject.AddComponent<T>();
		return c;
	}

	public static Component GetOrAddComponent(this GameObject gameobject, Type type)
	{
		Component c = gameobject.GetComponent(type);
		if (c) return c;
		return gameobject.AddComponent(type);
	}

	public static void RemoveComponent<T>(this Component component) where T: Component
	{
		T c;
		c = component.GetComponent<T>();
		if (c)
			GameObject.Destroy(c);
	}

	public static void RemoveComponent<T>(this GameObject gameObject) where T: Component
	{
		T c;
		c = gameObject.GetComponent<T>();
		if (c)
			GameObject.Destroy(c); 
	}

    /// <summary>
    /// Returns first Component of type found within the root object's heirarchy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="gameObject"></param>
    /// <returns></returns>
    public static T GetFromHeirarchy<T>(this GameObject gameObject) where T : Component
    {
        return gameObject.transform.root.GetComponentInChildren<T>();
    }

    /// <summary>
    /// Returns first Component of type found within the root object's heirarchy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="gameObject"></param>
    /// <returns></returns>
    public static T GetFromHeirarchy<T>(this Component component) where T : Component
    {
        return component.transform.root.GetComponentInChildren<T>();
    }

    /// <summary>
    /// Returns all components of type found within the root objects heirarchy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    /// <returns></returns>
    public static T[] GetAllFromHeirarchy<T>(this Component component) where T : Component
    {
        return component.transform.root.GetComponentsInChildren<T>();
    }

    /// <summary>
    /// Returns all components of type found within the root objects heirarchy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    /// <returns></returns
    public static T[] GetAllFromHeirarchy<T>(this GameObject go) where T : Component
    {
        return go.transform.root.GetComponentsInChildren<T>();
    }

    /// <summary>
    /// Returns first component of type found within the root object's heirarchy.
    /// If none found, will add component to the root object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    /// <returns></returns>
    public static T GetOrAddFromHeirarchy<T>(this Component component) where T : Component
    {
        T value = component.transform.root.GetComponentInChildren<T>();
        if (!value)
            return component.transform.root.gameObject.AddComponent<T>();
        return value;
    }

    /// <summary>
    /// Returns first component of type found within the root object's heirarchy.
    /// If none found, will add the component to the root object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    /// <returns></returns>
    public static T GetOrAddFromHeirarchy<T>(this GameObject gameObject) where T : Component
    {
        T value = gameObject.transform.root.GetComponentInChildren<T>();
        if (!value)
            return gameObject.transform.root.gameObject.AddComponent<T>();
        return value;
    }
}

/*
#if UNITY_EDITOR
namespace Inspectors
{
    using UnityEditor;

    [CanEditMultipleObjects]
    [CustomEditor(typeof(MonoBehaviour), true)]
    class MonobehaviourInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script");
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
*/
