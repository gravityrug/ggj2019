﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

public static class EditorExtensions
{
    /// <summary>
    /// Creates a button 
    /// </summary>
    public static void OpenScriptButton(SerializedObject serializedObject, params GUILayoutOption[] options)
    {
        if (GUILayout.Button("Edit", options))
        {
            var script = serializedObject.FindProperty("m_Script").objectReferenceValue;
            AssetDatabase.OpenAsset(script, 8);
        }
    }
}
#endif