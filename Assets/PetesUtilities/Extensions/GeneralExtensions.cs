﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GeneralExtensions
{
    public static string AddSpaceBeforeCaps(this string Input)
    {
        return new string(Input.SelectMany((c, i) => i > 0 && char.IsUpper(c) && char.IsLower(Input[i-1]) ? new[] { ' ', c } : new[] { c }).ToArray());
    }

    public static bool Contains(this string Input, string value, bool ignoreCase)
    {
        if (ignoreCase)
            return Input.IndexOf(value, System.StringComparison.OrdinalIgnoreCase) >= 0;
        return Input.Contains(value);
    }
}
