﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathExtensions
{
    public static Vector3 DirectionAlongNormal(this Vector3 direction, Vector3 normal)
    { 
        return (direction - (Vector3.Dot(direction, normal)) * normal).normalized;
    }

    public static int abs(this int integer)
    {
        return ((integer ^ (integer >> 31)) - (1 >> 31));
    }

    public static float abs(this float number)
    {
        return number > 0 ? number : -number;
    }

    public static int clamp(this int number, int min, int max)
    {
        int lmin, lmax;
        if (min > max)
        {
            lmax = min;
            lmin = max;
        }
        else
        {
            lmin = min;
            lmax = max;
        }
        if (number < lmin) return lmin;
        if (number > lmax) return lmax;
        return number;
    }

    public static float clamp(this float number, float min, float max)
    {
        float lmin, lmax;
        if (min > max)
        {
            lmax = min;
            lmin = max;
        }
        else
        {
            lmin = min;
            lmax = max;
        }
        if (number < lmin) return lmin;
        if (number > lmax) return lmax;
        return number;

    }

    public static int min(this int number, int value)
    {
        return number < value ? number : value;
    }

    public static float min(this float number, float value)
    {
        return number < value ? number : value;
    }

    public static int max(this int number, int value)
    {
        return number > value ? number : value;
    }

    public static float max(this float number, float value)
    {
        return number > value ? number : value;
    }
}
