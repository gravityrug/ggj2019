﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorExtensions
{
    public static bool Button(string label, string tooltip)
    {
        var style = new GUIStyle(EditorStyles.helpBox);
        var gui = new GUIContent(label, tooltip);
        return GUILayout.Button(gui, style);
    }
}
