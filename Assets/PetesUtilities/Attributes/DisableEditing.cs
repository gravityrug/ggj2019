﻿using UnityEngine;

namespace Attributes
{
	public class DisableEditing : PropertyAttribute
	{
        public DisableEditing()
        {}
                
        /// <summary>
        /// Always : Always Disable Field
        /// PlayModeOnly : Only Disable During Application PlayMode
        /// EditModeOnly : Only Disable When Not in Application PlayMode
        /// </summary>
        public Setting setting = Setting.Always;
        
        public enum Setting
        {
            Always,
            PlayModeOnly,
            EditModeOnly
        }
	}
}

