﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Attributes
{
    /// <summary>
    /// Change bool tickbox to true false field
    /// </summary>
    public class TrueFalse : PropertyAttribute
    {
    }

    /// <summary>
    /// Change bool tickbox to yes no field
    /// </summary>
    public class YesNo : PropertyAttribute
    {
    }

    /// <summary>
    /// Change bool to enable disable field
    /// </summary>
    public class EnableDisable : PropertyAttribute
    {
    }
}
