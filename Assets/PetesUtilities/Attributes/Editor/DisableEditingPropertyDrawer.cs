﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(Attributes.DisableEditing))]
public class DisableEditingPropertyDrawer : PropertyDrawer
{
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		return EditorGUI.GetPropertyHeight(property, label);
	}

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
        Attributes.DisableEditing value = attribute as Attributes.DisableEditing;

        switch (value.setting)
        {
            case Attributes.DisableEditing.Setting.EditModeOnly:
                GUI.enabled = Application.isPlaying;
                break;
            case Attributes.DisableEditing.Setting.PlayModeOnly:
                GUI.enabled = !Application.isPlaying;
                break;
            default:
                GUI.enabled = false;
                break;
        }

		EditorGUI.PropertyField(position, property, label, true);
		GUI.enabled = true;
	}
}
