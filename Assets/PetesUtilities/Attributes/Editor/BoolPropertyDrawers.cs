﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[CustomPropertyDrawer(typeof(Attributes.TrueFalse))]
class TrueFalsePropertyDrawer : PropertyDrawer
{
    string[] values = new string[] { "False", "True"};

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType != SerializedPropertyType.Boolean)
        {
            EditorGUI.LabelField(position, "Attribute Unsupported Property");
            return;
        }

        int selection = property.boolValue ? 1 : 0;
        selection = EditorGUI.Popup(position, property.displayName, selection, values);
        property.boolValue = selection > 0;
    }
}

[CustomPropertyDrawer(typeof(Attributes.EnableDisable))]
class EnableDisablePropertyDrawer : PropertyDrawer
{
    string[] values = new string[] {"Disable", "Enable"};

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType != SerializedPropertyType.Boolean)
        {
            EditorGUI.LabelField(position, "Attribute Unsupported Property");
            return;
        }

        int selection = property.boolValue ? 1 : 0;
        selection = EditorGUI.Popup(position, property.displayName, selection, values);
        property.boolValue = selection > 0;
    }
}

[CustomPropertyDrawer(typeof(Attributes.YesNo))]
class YesNoPropertyDrawer : PropertyDrawer
{
    string[] values = new string[] { "No" , "Yes"};

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType != SerializedPropertyType.Boolean)
        {
            EditorGUI.LabelField(position, "Attribute Unsupported Property");
            return;
        }

        int selection = property.boolValue ? 1 : 0;
        selection = EditorGUI.Popup(position, property.displayName, selection, values);
        property.boolValue = selection > 0;
    }
}