﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Linq;

namespace SimpleNodeEditor
{
    public class NodeEditorWindow : EditorWindow
    {
        [MenuItem("Window/Open Node Editor")]
        public static void Open()
        {
            var window = GetWindow<NodeEditorWindow>();
            window.titleContent = new GUIContent("Node Editor");
        }

        public static NodeEditorWindow Open(Node node)
        {
            var window = GetWindow<NodeEditorWindow>();
            window.titleContent = new GUIContent("Node Editor");

            if (node != null)
            {
                window.BaseNode = node;
                window._offset = Vector2.zero;
            }
            return window;
        }

        NodeEditorSettings settings;

        private void OnEnable()
        {
            settings = Resources.Load("NodeEditorSettings") as NodeEditorSettings;
            snapping = settings.Snapping;

            ConnectionTexture = new Texture2D(1, 3, TextureFormat.RGB24, false);
            ConnectionTexture.SetPixel(0, 0, Color.white);
            ConnectionTexture.Apply();

            DefaultBackground = GUI.backgroundColor;
        }


        void UpdateSavedSettings()
        {
            if (BaseNode == null)
                return;
            settings.InstanceID = BaseNode.GetInstanceID();
            settings.CurrentGraphPosition = _offset;
            settings.CurrentGraphZoom = _zoom;
            settings.Snapping = snapping;

        }

        private void OnDisable()
        {
            DrawConnections = null;
        }

        Texture2D ConnectionTexture;
        
        enum State
        {
            None,
            Resize,
            Rename,
            DragNode,
            DragSelectedNodes,
            AttachConnection,
            SelectDragableNodes
        }

        class NodeData
        {
            public Node RefNode;
            public Rect DisplayRect;
        }

        Rect GraphRect;
        Rect InspectorRect;
        Rect PanelResizeRect;
        Rect NodeTraceRect;

        Vector2 _offset = Vector2.zero;
        float _zoom = .5f;
        float _ratio = .75f;
        State _state;
        public Node BaseNode;

        Node _inspectedNode;
        Color _inspectedNodeColor;
        int _selectedField;
        int _selectedIndex;
        System.Type _queryType;

        Color DefaultBackground;

        List<NodeData> _allNodeData = new List<NodeData>();
        List<Node> _selectedNodes = new List<Node>();

        bool snapping = false;

        Vector2 SelectWithinAreaStartPosition;

        private void OnGUI()
        {
            if (BaseNode == null)
            {
                BaseNode = FindObjectsOfType<Node>().Where(x => x.GetInstanceID() == settings.InstanceID).FirstOrDefault();
                _offset = settings.CurrentGraphPosition;
                _zoom = settings.CurrentGraphZoom;
                snapping = settings.Snapping;

                var style = new GUIStyle(GUI.skin.label);
                style.alignment = TextAnchor.MiddleCenter;
                var windowRect = new Rect(0, 0, Screen.width, Screen.height);
                GUI.Label(windowRect, "Currently No Base Node Selected", style);
                return;
            }

            copiedNodes = copiedNodes.Where(n => n != null).ToArray();
            if (_state == State.None)
                _selectedNodes.Clear();
            else _selectedNodes = _selectedNodes.Where(n => n != null).ToList();

            DrawGraph();
            DrawInspector();
            DrawPanelResizer();
            ProcessEvents();
            UpdateSavedSettings();
        }

        private void Update()
        {
            Repaint();
        }

        void DrawGraph()
        {
            NodeTraceRect = new Rect(0f, 0f, position.width * _ratio, 20f);
            GraphRect = new Rect(0f, 20f, position.width * _ratio, position.height - 20f);

            GUI.backgroundColor = new Color(.7f,.7f,.7f);
            GUI.Box(GraphRect, GUIContent.none);
            GUI.backgroundColor = DefaultBackground;

            DrawGrid(25f, new Color(0f, 0f, 0f, Mathf.Lerp(0, .25f, _zoom)));
            DrawGrid(100f, new Color(0f, 0f, 0f, Mathf.Lerp(0, .5f, _zoom)));
            DrawNodeSelector();
            DrawAllNodes();
            DrawNodeTrace();
        }

        void OpenAsSubNode(Node node)
        {
            Deselect();
            BaseNode = node;
            _offset = Vector2.zero;
            _inspectedNode = node;
        }

        Node rootNode;
        void DrawNodeTrace()
        {
            List<Node> BaseNodes = new List<Node>();
            var parent = BaseNode;
            while (parent != null)
            {
                rootNode = parent;
                BaseNodes.Add(parent);
                if (parent.transform.parent != null)
                    parent = parent.transform.parent.GetComponent<Node>();
                else
                {
                    parent = null;
                } 
            }
            BaseNodes.Reverse();

            List<string> names = new List<string>();
            foreach (var item in BaseNodes)
                names.Add(item.name);
            var chosenBase = GUI.Toolbar(NodeTraceRect, BaseNodes.Count - 1, names.ToArray());
            if (chosenBase < BaseNodes.Count - 1)
            {
                BaseNode = BaseNodes[chosenBase];
                _offset = Vector2.zero;

                Deselect();
            }

        }

        void ProcessEvents()
        {
            Event e = Event.current;

            switch (e.type)
            {
                case EventType.MouseDrag:
                    OnMouseDrag();
                    return;
                case EventType.ScrollWheel:
                    _zoom -= Mathf.Sign(e.delta.y) * .1f;
                    _zoom = Mathf.Clamp(_zoom, .1f, 2f);
                    return;
                case EventType.MouseDown:
                    OnMouseDown();
                    return;
                case EventType.MouseUp:
                    OnMouseUp();
                    return;
                case EventType.KeyDown:
                    PressKey();
                    return;
            }
        }

        void PressKey()
        {
            Event e = Event.current;
            var nodeAtMousePosition = GetNodeAtMousePosition();
            switch (e.keyCode)
            {
                case KeyCode.Escape:
                    Deselect();
                    return;
                case KeyCode.F2:
                    _state = State.Rename;
                    return;
                case KeyCode.S:
                    if (!e.control)
                        snapping = !snapping;
                    return;
                case KeyCode.C:
                    CopyNodes();
                    return;
                case KeyCode.D:
                        CopyNodes();
                        PasteNodes(e.mousePosition);
                    return;
                case KeyCode.V:
                    PasteNodes(e.mousePosition);
                    return;
                case KeyCode.X:
                    if (_selectedNodes.Count > 0)
                    {
                        RemoveSelectedNodes();
                    }
                    else RemoveNode(_inspectedNode);
                    return;
                case KeyCode.Delete:
                    if (_selectedNodes.Count > 0)
                    {
                        RemoveSelectedNodes();
                    }
                    else RemoveNode(_inspectedNode);
                    return;
                case KeyCode.B:
                    _offset = Vector2.zero;
                    _inspectedNode = BaseNode;
                    return;

                case KeyCode.Space:
                    if (_inspectedNode != null)
                        _offset = -GetPosition(_inspectedNode);
                    return;

                case KeyCode.F:
                    if (_inspectedNode != null)
                        _offset = -GetPosition(_inspectedNode);
                    return;

                case KeyCode.H:
                    HideConnections();
                    return;

                case KeyCode.J:
                    if (_allNodeData.Count > 0)
                    {
                        List<Node> allnodes = new List<Node>();
                        foreach (var node in _allNodeData)
                            allnodes.Add(node.RefNode);
                        Undo.RecordObjects(allnodes.ToArray(), "Hide Fields");
                        for (int i = 0; i < allnodes.Count; ++i)
                        {
                            typeof(Node).GetField("hideConnections", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(allnodes[i], false);
                        }
                    }
                    return;
                case KeyCode.P:
                    if (_inspectedNode != null)
                        PingAndSelectNode(_inspectedNode);
                    return;
            }
        }

        void PingAndSelectNode(Node node)
        {
            if (node != null)
            {
                EditorGUIUtility.PingObject(node);
                Selection.activeObject = node;
            }
        }

        void HideConnections()
        {
            var nodeAtMousePosition = GetNodeAtMousePosition();
            if (_selectedNodes.Count > 0 && (_selectedNodes.Contains(nodeAtMousePosition) || nodeAtMousePosition == null))
            {
                Undo.RecordObjects(_selectedNodes.ToArray(), "Hide Fields");
                for (int i = 0; i < _selectedNodes.Count; ++i)
                {
                    ToggleHideConnection(_selectedNodes[i]);
                }
            }
            else if (nodeAtMousePosition != null)
            {
                var node = nodeAtMousePosition;
                Undo.RecordObject(node, "Hide Fields");            
                ToggleHideConnection(node);
            }        
        }

        void ToggleHideConnection(Node node)
        {
            var hideField = typeof(Node).GetField("hideConnections", BindingFlags.Instance | BindingFlags.NonPublic);
            hideField.SetValue(node, !(bool)hideField.GetValue(node));
        }

        void SetPosition(Node node, Vector2 position)
        {
            typeof(Node).GetField("position", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(node, position);
        }

        Vector2 GetPosition(Node node)
        {
            return (Vector2)typeof(Node).GetField("position", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(node);
        }

        Vector2 AddPosition(Node node, Vector2 addedPosition)
        {
            var field = typeof(Node).GetField("position", BindingFlags.Instance | BindingFlags.NonPublic);
            var positon = (Vector2)field.GetValue(node) + addedPosition;
            field.SetValue(node, position);
            return positon;
        }

        bool GetHideConnectionValue(Node node)
        {
            return (bool)typeof(Node).GetField("hideConnections", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(node);
        }

        void Deselect()
        {
            _inspectedNode = null;
            _state = State.None;
            _selectedField = -1;
            _selectedIndex = -1;
            _queryType = null;
            _selectedNodes.Clear();
            renameInspectorHeader = false;
            DragNodeGraph = false;
        }

        bool DragNodeGraph;
        void OnMouseDown()
        {
            var e = Event.current;
            var mousePos = e.mousePosition;
            var NodeAtMousePosition = GetNodeAtMousePosition();
        
            if (GraphRect.Contains(mousePos) && e.button == 2)
            {            
                DragNodeGraph = true;
                return;
            }

            if (e.button == 1) // On Right Click
            {
                DrawGraphContextMenu();
                return;
            }

            if (e.button == 0) // On Left Click
            {
                if (_state == State.DragSelectedNodes)
                {
                    if (GraphRect.Contains(mousePos) && _selectedNodes.Contains(NodeAtMousePosition))
                    {
                        return;
                    }
                }

                if (PanelResizeRect.Contains(mousePos))
                {
                    _state = State.Resize;
                    return;
                }

                if (NodeAtMousePosition != null)
                {
                    Deselect();
                    _inspectedNode = NodeAtMousePosition;
                    _state = State.DragNode;
                    return;
                }
            
                if (GraphRect.Contains(mousePos))
                {
                    Deselect();
                    _state = State.SelectDragableNodes;
                    SelectWithinAreaStartPosition = mousePos;
                }
            }
               
        }

        void OnMouseUp()
        {
            Event e = Event.current;

            if (e.button == 2)
            {
                DragNodeGraph = false;
                return;
            }

            if (_state == State.SelectDragableNodes && e.button == 0)
            {
                if (_selectedNodes.Count == 0)
                {
                    Deselect();
                    return;
                }
                _state = State.DragSelectedNodes;
                return;
            }
            if (_state == State.DragSelectedNodes && e.button == 0)
            {
                return;
            }

            _state = State.None;
        }

        void OnMouseDrag()
        {
            var e = Event.current;
            var mousePos = e.mousePosition;

            if (DragNodeGraph)
            {
                _offset += e.delta / _zoom;
            }

            switch (_state)
            {
                case State.Resize:
                    _ratio = Mathf.Clamp(e.mousePosition.x / position.width, .1f, .9f);
                    return;
                
                case State.DragNode:
                    if (_inspectedNode == BaseNode)
                        return;
                    Undo.RecordObject(_inspectedNode, "MovePosition");

                    SetPosition(_inspectedNode, GetPosition(_inspectedNode) + e.delta / _zoom);
                    return;

                case State.DragSelectedNodes:
                    if (DragNodeGraph) return;
                    Undo.RecordObjects(_selectedNodes.ToArray(), "MovePosition");
                    foreach (var node in _selectedNodes)
                    {
                        SetPosition(node, GetPosition(node) + e.delta / _zoom);
                    }
                    return;
            }
        }

        Vector2 SnapToGrid(Vector2 position)
        {
            if (!snapping)
                return position;

            var x = position.x - position.x % 25;
            var y = position.y - position.y % 25;
            return new Vector2(x, y);
        }

        void DrawNodeSelector()
        {
            if (_state != State.SelectDragableNodes)
                return;

            Event e = Event.current;

            _selectedNodes.Clear();

            var position = (e.mousePosition + SelectWithinAreaStartPosition) / 2f;
            var size = (e.mousePosition - SelectWithinAreaStartPosition);

            size.x = Mathf.Abs(size.x);
            size.y = Mathf.Abs(size.y);

            position.x -= size.x / 2f;
            position.y -= size.y / 2f;

            var bgColor = GUI.backgroundColor;
            GUI.backgroundColor = new Color(0, 0, 0, .4f);
            var selectionRect = new Rect(position, size);

            if (selectionRect.xMin < GraphRect.xMin)
                selectionRect.xMin = GraphRect.xMin;
            if (selectionRect.xMax > GraphRect.xMax)
                selectionRect.xMax = GraphRect.xMax;
            if (selectionRect.yMin < GraphRect.yMin)
                selectionRect.yMin = GraphRect.yMin;
            if (selectionRect.yMax > GraphRect.yMax)
                selectionRect.yMax = GraphRect.yMax;
        

            GUI.Box(selectionRect, "");

            foreach (var node in _allNodeData)
            {
                if (node.RefNode != BaseNode && selectionRect.Overlaps(node.DisplayRect))
                {
                    _selectedNodes.Add(node.RefNode);
                }
            }
            GUI.backgroundColor = bgColor;
        }

        void DrawGraphContextMenu()
        {
            GenericMenu contextMenu = new GenericMenu();

            // if Right Click On Node
            var node = GetNodeAtMousePosition();
            var mousePosition = Event.current.mousePosition;
            if (!GraphRect.Contains(mousePosition))
                return;

            if (node != null)
            {
                contextMenu.AddItem(new GUIContent("Select Node In Heirarchy"), false, () => PingAndSelectNode(node));
                contextMenu.AddItem(new GUIContent("Open as Subnode"), false, () => OpenAsSubNode(node));
                contextMenu.AddItem(new GUIContent("Toggle Show Connection"), false, () => { Undo.RecordObject(node, "Hide Fields"); ToggleHideConnection(node);});
                contextMenu.AddItem(new GUIContent("Remove Node"), false, () => RemoveNode(node));
            }

            if (copiedNodes.Length > 0)
                contextMenu.AddItem(new GUIContent("Paste Copied Nodes"), false, () => PasteNodes(mousePosition));

            if (_selectedNodes.Count > 0)
            {
                _state = State.None;
                contextMenu.AddItem(new GUIContent("Copy Selected Nodes"), false, () => CopyNodes());
                contextMenu.AddItem(new GUIContent("Duplicate Selected Nodes"), false, () => { CopyNodes(); PasteNodes(mousePosition);});
                contextMenu.AddItem(new GUIContent("Remove Selected Nodes"), false, () => RemoveSelectedNodes());
            }



            if (node == null)
            {
                // if Right Click Graph
            
                var list = new List<Node>(copiedNodes);
                list.RemoveAll(x => x == null);
                copiedNodes = list.ToArray();

                if (GraphRect.Contains(Event.current.mousePosition))
                {
                    var attr = rootNode.GetType().GetCustomAttributes(typeof(Attributes.AddNodeMenu), false)[0] as Attributes.AddNodeMenu;
                    foreach (var nodeType in attr.types)
                    {
                        contextMenu.AddItem(new GUIContent("Add "+nodeType.Name), false, () => AddNode(nodeType, mousePosition));
                    }
                }
            }
            contextMenu.ShowAsContext();
        }

        void AddNode(Type type, Vector2 position)
        {
            var newNode = new GameObject().AddComponent(type) as Node;
            newNode.transform.parent = BaseNode.transform;
            newNode.name = "New " + newNode.GetType().Name;
            SetPosition(newNode, ToNodePosition(position));
        }

        Node[] copiedNodes = new Node[] { };
        void CopyNodes()
        {
            copiedNodes = new Node[] { };
            if (_selectedNodes.Count > 0)
            {
                copiedNodes = _selectedNodes.ToArray();
            }

            Selection.objects = copiedNodes.Select( node => node.gameObject).ToArray<GameObject>();
            UnityEditor.Unsupported.CopyGameObjectsToPasteboard();
        }

        void PasteNodes(Vector2 position)
        {
            position = ToNodePosition(position);

            if (copiedNodes.Length > 0)
            {

                UnityEditor.Unsupported.DuplicateGameObjectsUsingPasteboard();

                var PastedNodes = Selection.gameObjects;

                if (PastedNodes.Length > 0)
                {
                    Vector2 offset = Vector2.zero;
                    var refNode = PastedNodes[0].GetComponent<Node>();
                    var pos = GetPosition(refNode);

                    offset = position - pos - new Vector2(0, 100);

                    _selectedNodes.Clear();
                    foreach (var go in PastedNodes)
                    {
                        var node = go.GetComponent<Node>();
                        node.transform.parent = BaseNode.transform;
                        go.name = go.name.Remove(go.name.Length - 4);
                        SetPosition(node, GetPosition(node) + offset);
                        _selectedNodes.Add(node);
                        SetPosition(node, GetPosition(node) + new Vector2(0, 100));
                    }
                    _state = State.DragSelectedNodes;
                }
            }       
        }

        void RemoveNode(Node node)
        {
            if (node == null)
                return;
            if (EditorUtility.DisplayDialog("Remove Nodes", string.Format("Permanently Remove {0}?", node.name), "Remove", "Cancel"))
            {
                DestroyImmediate(node.gameObject);
                Deselect();
            }        
        }

        void RemoveSelectedNodes()
        {
            if (_selectedNodes.Count > 0)
            {
                if (EditorUtility.DisplayDialog("Remove Nodes", "Permanently Remove Selected Nodes?", "Remove", "Cancel"))
                {
                    for (int i = 0; i < _selectedNodes.Count; ++i)
                    {
                        DestroyImmediate(_selectedNodes[i].gameObject);
                    }
                    Deselect();
                }
            }
        }

        void DrawAllNodes()
        {
            if (Event.current.type == EventType.MouseDown && Event.current.button == 2)
                return;

            // Get All Nodes
            _allNodeData.Clear();
            var AllNodes = new List<Node>();
            {
                foreach (Transform child in BaseNode.transform)
                {
                    var childNode = child.GetComponent<Node>();
                    if (childNode != null)
                        AllNodes.Add(childNode);
                }
                AllNodes.Add(BaseNode);
            }
                
            // Draw Connections
            if (DrawConnections != null)
                DrawConnections();
            DrawConnections = null;


            // Set up display rects
            foreach (var node in AllNodes)
            {
                Rect rect;
                if (node == BaseNode)
                    rect = new Rect(ToWindowPosition(Vector2.zero), new Vector2(300, 50) * _zoom);
                else
                {
                    rect = new Rect(ToWindowPosition(SnapToGrid(GetPosition(node))), new Vector3(300, 50) * _zoom);
                    //rect = new Rect(ToWindowPosition(SnapToGrid(node.position)), new Vector2(300, 50) * _zoom);
                }
                _allNodeData.Add(new NodeData() { DisplayRect = rect, RefNode = node });

            }

            // Process All Nodes
            foreach (var nodeData in _allNodeData)
            {
                var node = nodeData.RefNode;
                if (node == null)
                    continue;


                Color nodeColor = new Color(.7f, .7f, .7f); ;
                {
                    var colorAttribute = node.GetType().GetCustomAttributes(typeof(Attributes.NodeColor), false);
                    if (colorAttribute.Length > 0)
                    {
                        var attr = (colorAttribute[0] as Attributes.NodeColor);
                        nodeColor = new Color(attr.red, attr.green, attr.blue);
                    }

                    // Node Highlighting
                    if (Application.isPlaying && node != BaseNode)
                    {
                        var highlightField = BaseNode.GetType().GetFields(BindingFlags.Instance|BindingFlags.Public|BindingFlags.NonPublic).FirstOrDefault(x => x.IsDefined(typeof(Attributes.HighLightNode), false));
                        if (highlightField != null)
                        {
                            bool highlight = false;

                            if (typeof(IList).IsAssignableFrom(highlightField.FieldType))
                            {
                                var listValue = highlightField.GetValue(BaseNode) as IList;
                                if (listValue != null)
                                {
                                    foreach (var item in listValue)
                                    {
                                        if (item as Node == node)
                                        {
                                            highlight = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var highlightNode = highlightField.GetValue(BaseNode) as Node;

                                if (highlightNode == node)
                                {
                                    highlight = true;
                                }
                            }
                            if (highlight)
                            {
                                var attr = highlightField.GetCustomAttributes(typeof(Attributes.HighLightNode), false)[0] as Attributes.HighLightNode;
                                nodeColor = new Color(attr.red, attr.green, attr.blue);
                            }
                        }
                    }
                }
            
                Rect rect = nodeData.DisplayRect;
            
                float height = 50 * _zoom;

                var style = new GUIStyle(EditorStyles.miniButtonMid);//GUI.skin.box);
            
                style.fontSize = (int)(20f * _zoom);
                style.alignment = TextAnchor.MiddleCenter;

                // DrawHeader
                {
                    if (node == BaseNode)
                        nodeColor = Color.red; //new Color(1f, .3f, .3f);

                    if ((_state == State.DragSelectedNodes || _state == State.SelectDragableNodes) && _selectedNodes.Contains(node))
                        nodeColor = Color.yellow;
                    if (_queryType != null)
                    {
                        if (_queryType.IsAssignableFrom(node.GetType()) && _inspectedNode != node && node != BaseNode)
                        {
                            if (rect.Contains(Event.current.mousePosition))
                                nodeColor = Color.red;
                            else
                                nodeColor = Color.cyan;
                        }
                        else
                            nodeColor = Color.grey;
                    }

                    GUI.backgroundColor = nodeColor;
                    style.fontStyle = FontStyle.Bold;
                                
                    // Rename Node
                    if (_state == State.Rename && _inspectedNode == node)
                    {
                        GUI.SetNextControlName("Input");
                        node.name = GUI.TextField(rect, node.name, style);
                        EditorGUI.FocusTextInControl("Input");

                        if (Event.current.type == EventType.KeyDown)
                        {
                            _state = State.None;
                        }                    
                    }
                    else
                    {
                        style.normal.textColor = Color.white;
                        GUI.Box(rect, node.name, style);
                    }

                    if (_inspectedNode == node)
                        _inspectedNodeColor = GUI.backgroundColor;

                    if (node != _inspectedNode && node != BaseNode && _queryType != null &&  _queryType.IsAssignableFrom(node.GetType()))
                    {
                        EditorGUIUtility.AddCursorRect(rect, MouseCursor.ArrowPlus);
                    }
                    else if (node != BaseNode)
                    {
                        EditorGUIUtility.AddCursorRect(rect, MouseCursor.Pan);
                    }
                }
                rect.y += height;
                style.normal.textColor = Color.black;
                //style.fontStyle = FontStyle.Normal;
                GUI.backgroundColor = DefaultBackground;

                // Get Fields and filter out any that are unecessary  
                var fields = node.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public).Where(x => x.IsDefined(typeof(Attributes.NodeField), false)).ToArray();
                {
                    var tempFields = new List<FieldInfo>();
                    foreach (var field in fields)
                    {
                        var attribute = (field.GetCustomAttributes(typeof(Attributes.NodeField), false))[0] as Attributes.NodeField;
                        switch (attribute.ShowType)
                        {
                            case ShowNodeField.ChildNodeOnly:
                                if (node != BaseNode)
                                    tempFields.Add(field);
                                break;
                            case ShowNodeField.BaseNodeOnly:
                                if (node == BaseNode)
                                    tempFields.Add(field);
                                break;
                            default:
                                tempFields.Add(field);
                                break;
                        }
                    }
                    fields = tempFields.ToArray();
                }

                style.fontSize = (int)(18f * _zoom);


                // Iterate over all fields
                for (int fieldPosition = 0; fieldPosition < fields.Length; ++fieldPosition)
                {
                    var field = fields[fieldPosition];
                    var fieldValue = field.GetValue(node);

                    // Draw as Node Field
                    if ( typeof(Node).IsAssignableFrom(field.FieldType))
                    {
                        var fieldNode = fieldValue as Node;

                        if (fieldNode != null && AllNodes.Contains(fieldNode) && !GetHideConnectionValue(node))
                        {
                            AddConnection(nodeColor, new Vector2(rect.x + rect.width, rect.y + height / 2f), ToWindowPosition(SnapToGrid( GetPosition(fieldNode))) + new Vector2(0, height / 2f));
                        }

                        if (fieldPosition == _selectedField && _inspectedNode == node)
                        {
                            GUI.backgroundColor = Color.cyan;
                            AddConnection(Color.cyan, new Vector2(rect.x + rect.width, rect.y + height / 2f), Event.current.mousePosition);

                            _queryType = field.FieldType;
                            GUI.backgroundColor = Color.cyan;

                            var e = Event.current;

                            if (e.type == EventType.MouseDown && e.button == 0)
                            {
                                var ToNode = GetNodeAtMousePosition();
                                if (ToNode != null && ToNode != node && ToNode != BaseNode && _queryType.IsAssignableFrom(field.FieldType))
                                {
                                    Undo.RecordObject(node, "SetField");
                                    field.SetValue(node, ToNode);
                                }
                                else
                                {
                                    Undo.RecordObject(node, "SetField");
                                    field.SetValue(node, null);
                                }
                                _selectedField = -1;
                                _queryType = null;
                                e.Use();
                            }
                        }
                        string fieldName = "Empty";

                        if (fieldNode != null)
                        {
                            fieldName = fieldNode.name;
                        }
                        if (GraphRect.Overlaps(rect) && GUI.Button(rect, new GUIContent(fieldName, fieldName), style))
                        {
                            _selectedField = fieldPosition;
                            _inspectedNode = node;

                        }
                        GUI.backgroundColor = DefaultBackground;
                        rect.y += height;
                    }
                    // Draw as List Field
                    else if (typeof(IList).IsAssignableFrom(field.FieldType))
                    {
                        var ListField = fieldValue as IList;
                        if (ListField == null)
                        {
                            ListField = Activator.CreateInstance(field.FieldType) as IList;
                            field.SetValue(node, ListField);
                        }

                        Type queryType = field.FieldType.GetGenericArguments()[0];
                    
                        if (_selectedField == fieldPosition && _inspectedNode == node)
                            GUI.backgroundColor = Color.cyan;

                        string fieldTitle = field.Name;
                        if (ListField.Count > 0)
                        {
                            fieldTitle = ListField.Count + " " + fieldTitle;
                            foreach (var item in ListField)
                            {
                                if (item == null)
                                    style.normal.textColor = Color.red;                            
                            }
                        }

                        // Draw List Field
                        if (GraphRect.Overlaps(rect) && GUI.Button(rect, fieldTitle, style))
                        {
                            _selectedField = fieldPosition;
                            _inspectedNode = node;
                        }
                        GUI.backgroundColor = DefaultBackground;
                        rect.y += height;
                        style.normal.textColor = Color.black;

                        // Draw Indexes 
                        for (int indexPosition = 0; indexPosition < ListField.Count; ++indexPosition)
                        {
                            var indexValue = ListField[indexPosition] as Node;
                            string fieldName;
                            if (indexValue == null)
                            {
                                fieldName = "Empty";
                                style.normal.textColor = Color.red;
                            }
                            else fieldName = indexValue.name;

                            if (node == _inspectedNode && _selectedField == fieldPosition)
                            {
                                if (_selectedIndex == indexPosition)
                                {
                                    _queryType = queryType;
                                    GUI.backgroundColor = Color.cyan;
                                    AddConnection(Color.cyan, new Vector2(rect.x + rect.width, rect.y + height / 2f), Event.current.mousePosition);

                                    var e = Event.current;

                                    if (e.type == EventType.MouseDown && e.button == 0)
                                    {
                                        var ToNode = GetNodeAtMousePosition();
                                        if (ToNode != null && ToNode != node && ToNode != BaseNode && queryType.IsAssignableFrom(ToNode.GetType()))
                                        {
                                            Undo.RecordObject(node, "Set Field");
                                            ListField[indexPosition] = ToNode;
                                        }
                                        else
                                        {
                                            Undo.RecordObject(node, "Set Field");
                                            ListField[indexPosition] = null;
                                        }
                                        _selectedIndex = -1;
                                        _queryType = null;
                                        e.Use();
                                    }
                                }

                                float divisor = 12f;
                                float singleWidth = rect.width / divisor;
                            
                                if (GraphRect.Overlaps(rect) && GUI.Button(new Rect(rect.x + singleWidth*2f, rect.y, singleWidth*(divisor - 4f), rect.height) , new GUIContent(fieldName, fieldName), style))
                                {
                                    _selectedIndex = indexPosition;
                                }
                                GUI.backgroundColor = DefaultBackground;
                                if (GraphRect.Overlaps(rect) && GUI.Button(new Rect(rect.x, rect.y, singleWidth, rect.height), "-", style))
                                {
                                    ListField.RemoveAt(indexPosition);
                                    return;
                                }
                                if (GraphRect.Overlaps(rect) && GUI.Button(new Rect(rect.x+singleWidth, rect.y, singleWidth, rect.height), "+", style))
                                {
                                    ListField.Insert(indexPosition, null);
                                    return;
                                }
                                if (indexPosition == 0)
                                    GUI.enabled = false;
                                if (GraphRect.Overlaps(rect) && GUI.Button(new Rect(rect.x+singleWidth*(divisor - 2f), rect.y, singleWidth, rect.height), "↑", style))
                                {
                                    var prevItem = ListField[indexPosition - 1];
                                    ListField[indexPosition - 1] = indexValue;
                                    ListField[indexPosition] = prevItem;
                                }
                                GUI.enabled = true;
                                if (indexPosition == ListField.Count - 1)
                                    GUI.enabled = false;
                                if (GraphRect.Overlaps(rect) && GUI.Button(new Rect(rect.x+singleWidth*(divisor - 1f), rect.y, singleWidth, rect.height), "↓", style))
                                {
                                    var prevItem = ListField[indexPosition + 1];
                                    ListField[indexPosition + 1] = indexValue;
                                    ListField[indexPosition] = prevItem;
                                }
                                GUI.enabled = true;
                            

                                if (indexValue != null && AllNodes.Contains(indexValue) &&! GetHideConnectionValue(node))
                                {
                                    AddConnection(nodeColor, new Vector2(rect.x + rect.width, rect.y + height / 2f),    ToWindowPosition(SnapToGrid(GetPosition(indexValue))) + new Vector2(0, height / 2f));
                                }
                                rect.y += height;
                            }
                            else
                            {
                                if (indexValue != null && AllNodes.Contains(indexValue) && ! GetHideConnectionValue(node))
                                {
                                    AddConnection( nodeColor, new Vector2(rect.x + rect.width, rect.y - height / 2f),   ToWindowPosition(SnapToGrid(GetPosition(indexValue)) + new Vector2(0, height / 2f)));
                                }
                            }
                            style.normal.textColor = Color.black;
                        }
                        if (_inspectedNode == node && _selectedField == fieldPosition)
                        {
                            if (GraphRect.Overlaps(rect) && GUI.Button(rect, "Add Field", style))
                            {
                                ListField.Add(null);
                            }
                            rect.y += height;
                        }   
                    }
                }
            }
        
            GUI.backgroundColor = DefaultBackground;
            _allNodeData.Reverse();
        }
    
        int inspectorTab = 0;
        string[] inspectorTabs = new string[] { "Inspector", "Info" };
        Editor _selectedNodeEditor;
        bool renameInspectorHeader;
        void DrawInspector()
        {
            GUI.backgroundColor = DefaultBackground;
            InspectorRect = new Rect(position.width * _ratio, 0f, position.width * (1f - _ratio), position.height);
            GUI.Box(InspectorRect, GUIContent.none);

            var rect = new Rect(InspectorRect.x, InspectorRect.y, InspectorRect.width, 20f);
            if (_inspectedNode != null)
            {
                inspectorTab = GUI.Toolbar(rect, inspectorTab, inspectorTabs);
            }
            else
            {
                var style = new GUIStyle(EditorStyles.toolbar);
                style.alignment = TextAnchor.MiddleCenter;
                style.fontSize = 11;
                GUI.Label(rect, "Info", style);
                //GUI.Label(rect, "Info");
            }
            rect.y += 20f;
        

            var height = EditorGUIUtility.singleLineHeight;
            rect.height = height;

            if (inspectorTab == 0)
            {
                if (_inspectedNode != null)
                {
                    if (_selectedNodeEditor == null || _selectedNodeEditor.target as Node != _inspectedNode)
                    {
                        _selectedNodeEditor = Editor.CreateEditor(_inspectedNode);
                    }

                    {
                        rect.y += 2f;
                        rect.height = height*1.25f;

                        var prefab = PrefabUtility.GetCorrespondingObjectFromSource(rootNode);

                        var buttonRect = rect;
                        
                        if (prefab != null)
                            buttonRect.width /= 2f;
                                                
                        if (GUI.Button(buttonRect, new GUIContent("Open As Subnode", "Opens the node as subnode")))
                        {
                        OpenAsSubNode(_inspectedNode);
                            return;
                        }

                        if (prefab != null)
                        {
                            buttonRect.x += buttonRect.width;
                            if (GUI.Button(buttonRect, new GUIContent("Apply", "Apply changes to Saved Prefab")))
                            {
#pragma warning disable
                                GameObject go = PrefabUtility.FindValidUploadPrefabInstanceRoot(BaseNode.gameObject);
                                PrefabUtility.ReplacePrefab(go, prefab, ReplacePrefabOptions.ConnectToPrefab);
#pragma warning restore 
                            }
                        }
                        rect.y += height * 1.25f;                        
                    }

                    {// Draw Header
                        var HeaderStyle = new GUIStyle(EditorStyles.miniButton);
                        HeaderStyle.fontStyle = FontStyle.Bold;
                        HeaderStyle.normal.textColor = Color.white;
                        HeaderStyle.fontSize = 20;
                    
                        GUI.backgroundColor = _inspectedNodeColor;

                        rect.height = 36f;
                        rect.y += 2f;
                        rect.x += 2f;
                        rect.width -= 4f;

                        if (renameInspectorHeader)
                        {
                            GUI.SetNextControlName("HeaderRename");
                            _inspectedNode.name = GUI.TextField(rect, _inspectedNode.name, HeaderStyle);
                            EditorGUI.FocusTextInControl("HeaderRename");

                            if (Event.current.type == EventType.KeyDown || Event.current.type == EventType.MouseDown)
                            {
                                renameInspectorHeader = false;
                            }
                        }
                        else
                        {
                            if (GUI.Button(rect, _inspectedNode.name, HeaderStyle))
                            {
                                renameInspectorHeader = true;
                            }
                        }                       
                        
                        rect.y += 36f;
                        GUI.backgroundColor = DefaultBackground;

                        


                        rect = Rect.MinMaxRect(rect.xMin, rect.yMin, rect.xMax, InspectorRect.yMax);
                        
                    }

                    

                    BeginWindows();
                    GUI.Window(1, rect, (x) => 
                    {
                        var nodeInspector = _selectedNodeEditor as Inspectors.INodeEditor;
                        if (nodeInspector != null)
                            nodeInspector.OnNodeGUI();
                        else
                            _selectedNodeEditor.OnInspectorGUI();
                        }, GUIContent.none, GUIStyle.none);
                    EndWindows();
                }
            }         
            


            if (_inspectedNode == null || inspectorTab == 1)
            {
                rect = new Rect(rect.x + 12f, EditorGUIUtility.singleLineHeight * 2f, rect.width, EditorGUIUtility.singleLineHeight);

                {
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "### CONTROLS ###");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "Left Click : Select");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "Middle Click : Pan");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "Right Click : Context Menu");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "Scroll Wheel : Zoom");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "ESC : Deselect");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "F2  : Rename");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "S   : Snapping Mode");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "Del or X : Delete Nodes");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "C : Copy Nodes");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "V : Paste Nodes");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "D : Duplicate Nodes");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "B : Center Base Node");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "Space or F : Focus Inspected Node");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "H : Toggle Connection Visiblity");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "J : Show All Connections");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "P : Select Node in Heirarchy");
                    rect.y += EditorGUIUtility.singleLineHeight;
                }

                {
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, "### GRAPH INFO ###");
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("Offset: {0:0.##}x, {1:0.##}y", -_offset.x, _offset.y));
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("Zoom: {0:0.##}", _zoom));
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("Mouse Pos: {0}", ToNodePosition(Event.current.mousePosition)));
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("State: {0}", _state));
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("Snapping: {0}", snapping));
                    rect.y += EditorGUIUtility.singleLineHeight;            
                    EditorGUI.LabelField(rect, string.Format("All Nodes: {0}", _allNodeData.Count));
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("Selected Nodes: {0}", _selectedNodes.Count));
                    rect.y += EditorGUIUtility.singleLineHeight;

                    for (int i = 0; i < _selectedNodes.Count; ++ i)
                    {
                        var item = _selectedNodes[i];
                        if ( GUI.Button(rect, string.Format("    {0}: {1}", i, _selectedNodes[i].gameObject.name), EditorStyles.label))
                        {
                            _selectedNodes.Clear();
                            _selectedNodes.Add(item);
                            _inspectedNode = item;
                        }
                        rect.y += EditorGUIUtility.singleLineHeight;
                    }
                }
                if (_inspectedNode != null)
                {
                    rect.y += height;
                    EditorGUI.LabelField(rect, "### INSPECTED NODE ###");
                    rect.y += height;
                    EditorGUI.LabelField(rect, string.Format("Node: {0}", _inspectedNode == null ? null : _inspectedNode.name));
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("Node Position: {0}", GetPosition(_inspectedNode)));
                    rect.y += EditorGUIUtility.singleLineHeight;
                    EditorGUI.LabelField(rect, string.Format("Node Hide Connections: {0}", GetHideConnectionValue(_inspectedNode)));
                    rect.y += EditorGUIUtility.singleLineHeight;
                }


            }
        }

        void DrawPanelResizer()
        {
            PanelResizeRect = new Rect(position.width * _ratio - 5f, 0f, 10f, position.height);
            EditorGUIUtility.AddCursorRect(PanelResizeRect, MouseCursor.ResizeHorizontal);
        }

        void DrawGrid(float size, Color color)
        {
            Handles.BeginGUI();
            Handles.color = color;

            Vector2 min = GraphRect.min;
            Vector2 max = GraphRect.max;

            min = ToNodePosition(min) - new Vector2(size, size);
            max = ToNodePosition(max) + new Vector2(size, size);

            min.x -= min.x % size;
            min.y -= min.y % size;

            for (float x = min.x; x < max.x; x += size)
            {
                var xmin = ToWindowPosition(new Vector2(x, min.y));
                var xmax = ToWindowPosition(new Vector2(x, max.y));

                Handles.DrawLine(new Vector3(xmin.x, xmin.y, 0), new Vector3(xmax.x, xmax.y, 0));
            }
            for (float y = min.y; y < max.y; y += size)
            {
                var ymin = ToWindowPosition(new Vector2(min.x, y));
                var ymax = ToWindowPosition(new Vector2(max.x, y));

                Handles.DrawLine(new Vector3(ymin.x, ymin.y, 0), new Vector3(ymax.x, ymax.y, 0));
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }

        Vector2 ToNodePosition(Vector2 windowPosition)
        {
            windowPosition -= GraphRect.center;
            windowPosition /= _zoom;
            windowPosition -= _offset;
            return windowPosition;
        }

        Vector2 ToWindowPosition(Vector2 nodePosition)
        {
            nodePosition += _offset;
            nodePosition *= _zoom;
            nodePosition += GraphRect.center;
            return nodePosition;
        }

        Node GetNodeAtPosition(Vector2 windowPos)
        {
            foreach (var node in _allNodeData)
            {
                if (node.DisplayRect.Contains(Event.current.mousePosition))
                    return node.RefNode;
            }
            return null;
        }

        Node GetNodeAtMousePosition()
        {
            var mousePos = Event.current.mousePosition;
            if (!GraphRect.Contains(mousePos))
                return null;
            foreach (var node in _allNodeData)
            {
                if (node.DisplayRect.Contains(mousePos))
                    return node.RefNode;
            }
            return null;
        }

        Action DrawConnections = null;

        void AddConnection(Color color, Vector2 startPos, Vector2 endPos)
        {
            startPos.x -= 2f;
            endPos.x += 2f;

            var shadowColor = new Color(color.r/3f, color.g/3f, color.b/3f);
            var curveSize = 8f * _zoom;
            var bendFactor = 25f * _zoom;

            DrawConnections += () =>
            {
                Handles.DrawBezier(startPos, endPos, startPos + Vector2.right * bendFactor, endPos + Vector2.left * bendFactor, shadowColor, ConnectionTexture, curveSize * 1.4f);
                Handles.DrawBezier(startPos, endPos, startPos + Vector2.right * bendFactor, endPos + Vector2.left * bendFactor, color, ConnectionTexture, curveSize);
            };        
        }
    }
}
