﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SimpleNodeEditor;

namespace Inspectors
{
    [CustomEditor(typeof(Node), true)]
    class NodeInspector : Editor, INodeEditor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            if (GUILayout.Button("Open Editor"))
                NodeEditorWindow.Open(target as Node);
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }

        public void OnNodeGUI()
        {

        }
    }
}

