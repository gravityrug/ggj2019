﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SimpleNodeEditor
{
    //[CreateAssetMenu(menuName = "Node Editor Settings")]
    public class NodeEditorSettings : ScriptableObject
    {
        public int instanceID;
        public Vector2 CurrentGraphPosition;
        public float CurrentGraphZoom;
        public bool Snapping;

        public int InstanceID
        {
            get
            {
                return instanceID;
            }
            set
            {
                if (value != instanceID)
                {
                    EditorUtility.SetDirty(this);
                }
                instanceID = value;
            }
        }

    }
}
