﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShowNodeField
{
    BaseNodeOnly,
    ChildNodeOnly,
    Always
}

namespace Attributes
{
    /// <summary>
    /// Allows field to be assignable in the Node Editor
    /// </summary>
    public class NodeField : System.Attribute
    {
        public NodeField() { }

        /// <summary>
        /// Sets visability of the Field.
        /// Defaults to be shown on child nodes only.
        /// </summary>
        /// <param name="BaseNodeField"></param>
        public NodeField(ShowNodeField type)
        {
            ShowType = type;
        }

        /// <summary>
        /// Field is a BaseNode Field
        /// </summary>
        public ShowNodeField ShowType = ShowNodeField.ChildNodeOnly;
    }


    /// <summary>
    /// High Lights Node Field in the Node Editor when in Playmode if Node is Base Node
    /// Can only highlight a single Node field
    /// </summary>
    public class HighLightNode : System.Attribute
    {
        public HighLightNode()
        {}

        public HighLightNode(float red, float green, float blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        public float red = 0f;
        public float green = 1f;
        public float blue = 1f;
    }

    /// <summary>
    /// Sets the Node's default color in the Node Editor
    /// </summary>
    public class NodeColor : System.Attribute
    {
        public NodeColor(float red, float green, float blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        public float red;
        public float green;
        public float blue;
    }

    /// <summary>
    /// When Node is root node, these nodes show up on the add node context menu
    /// </summary>
    public class AddNodeMenu : System.Attribute
    {
        public AddNodeMenu(params Type[] types)
        {
            this.types = types;
        }

        public Type[] types;
    }
}
