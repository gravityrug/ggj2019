﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Attributes.NodeColor(0f, .7f, .7f)]
public abstract class Node : MonoBehaviour
{
#if UNITY_EDITOR
#pragma warning disable
    [SerializeField, HideInInspector]
    Vector2 position = new Vector2();
    [SerializeField, HideInInspector]
    bool hideConnections = false;
#pragma warning restore
#endif
}

#if UNITY_EDITOR
namespace Inspectors
{
    using System;
    using System.Linq;
    using UnityEditor;

    // Node Editor will interface to render it's inspector instead of the default OnInspectorGUI
    public interface INodeEditor
    {
        void OnNodeGUI();
    }
       

}
#endif
