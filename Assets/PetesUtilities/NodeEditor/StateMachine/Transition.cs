﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachineNode
{
    [Attributes.NodeColor(.8f, .1f, .2f)]
    public class Transition : Node
    {
        [Attributes.NodeField]
        public State ToState;

        [ContextMenu("Initialize")]
		void Awake()
		{
            InitializedConditions = GetComponents<IBeginConditionCheck>();
			EvaluatingConditions = GetComponents<Condition>();
            CleanUpConditions = GetComponents<IEndConditionCheck>();
		}

		public bool Evaluate()
		{
            if (ToState == null) return false;
			foreach(var condition in EvaluatingConditions)
			{
				if (!condition.Evaluate())
					return false;
			}
			return true;
		}

        public void Initialize()
        {
            if (InitializedConditions != null && InitializedConditions.Length > 0)
            {
                foreach (var condition in InitializedConditions)
                    condition.BeginCheck();
            }
        }

        public void CleanUp()
        {
            if (CleanUpConditions != null && CleanUpConditions.Length > 0)
            {
                foreach (var condition in CleanUpConditions)
                    condition.EndCheck();
            }
        }

        public IBeginConditionCheck[] InitializedConditions;
        public IEndConditionCheck[] CleanUpConditions;
		[System.NonSerialized]
		public Condition[] EvaluatingConditions;
	}
}

#if UNITY_EDITOR
namespace Inspectors
{
    using StateMachineNode;
    using UnityEditor;

    [CustomEditor(typeof(Transition), true)]
    class TransitionInspector : Editor, INodeEditor
    {
        DrawAddComponentFields<Condition> Conditions;

        void OnEnable()
        {
            Conditions = new DrawAddComponentFields<Condition>(typeof(Transition), target, serializedObject);
        }
        
        public void OnNodeGUI()
        {
            Conditions.OnNodeGUI();
        }
    }
}
#endif