﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachineNode
{
    [Attributes.AddNodeMenu(typeof(State), typeof(Transition))]
    [AddComponentMenu("Actor/StateMachine")]
	public class StateMachine : Node, ICurrentState
	{
		[Attributes.NodeField(ShowNodeField.BaseNodeOnly)]
		public State InitialState;

        [Attributes.DisableEditing]
        [Attributes.HighLightNode]
        public State currentState;

        public State CurrentState()
        {
            return currentState;
        }

        void FixedUpdate()
        {
            if (currentState)
                currentState.FixedState();
        }

		void LateUpdate()
		{
            if (!currentState)
            {
                if (!InitialState)
                {
                    enabled = false;
                    Debug.LogError("Initial State Must be supplied", this);
                    return;
                }
                currentState = InitialState;
                currentState.EnterState();
                return;
            }

            currentState.UpdateState();
			foreach(var transition in currentState.Transitions)
			{
                if (transition == null)
                    continue;
				if (transition.Evaluate())
				{
					currentState.ExitState();
					currentState = transition.ToState;
					currentState.EnterState();
					return;
				}	
			}
		}
    }

    public interface ICurrentState
    {
        State CurrentState();
    }
}

