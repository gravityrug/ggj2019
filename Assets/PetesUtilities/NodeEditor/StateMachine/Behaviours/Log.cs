using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateBehaviours.Misc
{
	public class Log : StateBehaviour, IEnterState, IHelpInfo
	{
        public string Message;
		public void EnterState ()
		{
            Debug.Log(Message, this);		
		}

        public string HelpInfo()
        {
            return "Logs Message when Entering State";
        }
    }
}


