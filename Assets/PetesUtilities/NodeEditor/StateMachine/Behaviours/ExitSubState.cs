using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateBehaviours.Misc
{
    public class ExitSubState : StateBehaviour, IEnterState, IHelpInfo
    {
        void Start()
        {
            subState = transform.parent.GetComponent<StateMachineNode.State>();

            if (subState == null)
                Debug.LogErrorFormat(this, "No parent Substate for {0}", name);
        }

        StateMachineNode.State subState;

        public void EnterState()
        {
            subState.Exit = true;
        }

        public string HelpInfo()
        {
            return "Set's the state's Exit condition to true.";
        }
    }
}