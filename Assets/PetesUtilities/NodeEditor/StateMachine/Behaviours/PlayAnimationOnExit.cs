using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateBehaviours.Animation
{
    public class PlayAnimationOnExit : StateBehaviour, IExitState
    {
        public string state;
        public int layer;
        [Range(0f, 1f)]
        public float crossFade = .2f;

        Animator animator;

        public int hash;

        void Start()
        {
            animator = transform.root.GetComponentInChildren<Animator>();
            hash = Animator.StringToHash(state);
        }

        public void ExitState()
        {
            animator.CrossFade(hash, crossFade, layer, 0f, 0f);
        }
    }
}

# if UNITY_EDITOR

namespace Inspectors
{
    using UnityEditor;
    using UnityEditor.Animations;
    using StateBehaviours.Animation;

    [CustomEditor(typeof(PlayAnimationOnExit))]
    public class PlayAnimationOnExitEditor : Editor
    {

        PlayAnimationOnExit anim;
        AnimatorController controller;


        private void OnEnable()
        {
            anim = (PlayAnimationOnExit)target;

            var animator = anim.transform.root.GetComponentInChildren<Animator>();
            controller = animator.runtimeAnimatorController as AnimatorController;

            layerNames = new string[controller.layers.Length];
            for (int itr = 0; itr < controller.layers.Length; ++itr)
            {
                layerNames[itr] = controller.layers[itr].name;
                var stateNames = new string[controller.layers[itr].stateMachine.states.Length];

                for (int i = 0; i < stateNames.Length; ++i)
                {
                    stateNames[i] = controller.layers[itr].stateMachine.states[i].state.name;
                }

                states.Add(stateNames);
            }
        }

        int selection;
        string[] layerNames;
        List<string[]> states = new List<string[]>();

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            if (anim.layer >= states.Count)
                anim.layer = 0;

            anim.layer = EditorGUILayout.Popup("Layer", anim.layer, layerNames);

            var stateNames = states[anim.layer];

            selection = System.Array.IndexOf(states[anim.layer], anim.state);

            if (selection < 0)
                selection = 0;

            if (stateNames.Length > 0)
            {
                selection = EditorGUILayout.Popup("State", selection, stateNames);
                anim.state = stateNames[selection];
                anim.hash = Animator.StringToHash(anim.state);
                var crossfade = serializedObject.FindProperty("crossFade");
                EditorGUILayout.PropertyField(crossfade);
            }
            else
            {
                EditorGUILayout.HelpBox("NO STATES IN LAYER", MessageType.Error);
            }

            serializedObject.ApplyModifiedProperties();

        }
    }
}
#endif
