using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateBehaviours.Animation
{
    public class SetAnimatorFloatsUsingInputAxis : StateBehaviour, IUpdateState
    {
        Animator Animator;
        ControllerInput input;

        public List<Settings> settings = new List<Settings>();

        void Start()
        {
            var root = transform.root;
            Animator = root.GetComponentInChildren<Animator>();
            input = root.GetComponentInChildren<ControllerInput>();

            foreach (var item in settings)
            {
                item.hash = Animator.StringToHash(item.parameter);
            }
        }

        public void UpdateState()
        {
            for (int i = 0; i < settings.Count; ++i)
            {
                var setting = settings[i];

                Animator.SetFloat(setting.hash, input.GetAxis(setting.axis));
            }
        }

        [System.Serializable]
        public class Settings
        {
            public Inputs.Axis axis;
            public string parameter;
            public int hash;
        }
    }
}

#if UNITY_EDITOR

namespace Inspectors
{
    using UnityEditor;
    using UnityEditor.Animations;
    using StateBehaviours.Animation;

    [CustomEditor(typeof(SetAnimatorFloatsUsingInputAxis))]
    class SetAnimatorFloatUsingInputAxisInspector : Editor
    {
        SetAnimatorFloatsUsingInputAxis obj;


        private void OnEnable()
        {
            obj = target as SetAnimatorFloatsUsingInputAxis;

            var animator = obj.transform.root.GetComponentInChildren<Animator>();
            if (!animator)
                return;
            var controller = animator.runtimeAnimatorController as AnimatorController;

            foreach (var parameter in controller.parameters)
            {
                if (parameter.type == AnimatorControllerParameterType.Float)
                {
                    parameters.Add(parameter.name);
                }
            }
        }

        List<string> parameters = new List<string>();

        public override void OnInspectorGUI()
        {
            if (parameters.Count == 0)
            {
                EditorGUILayout.HelpBox("NO FLOAT PARAMETERS ON ANIMATOR CONTROLLER", MessageType.Error);
                return;
            }

            serializedObject.Update();

            if (obj.settings.Count == 0)
            {
                obj.settings.Add(new SetAnimatorFloatsUsingInputAxis.Settings());
            }

            for (int i = 0; i < obj.settings.Count; ++i)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    var setting = obj.settings[i];

                    int selection = System.Array.IndexOf(parameters.ToArray(), setting.parameter);

                    if (selection < 0) selection = 0;

                    setting.axis = (Inputs.Axis)EditorGUILayout.EnumPopup(setting.axis);
                    selection = EditorGUILayout.Popup(selection, parameters.ToArray());
                    setting.parameter = parameters[selection];

                    if (GUILayout.Button("+", GUILayout.MaxWidth(16f)))
                    {
                        obj.settings.Insert(i, new SetAnimatorFloatsUsingInputAxis.Settings());
                        EditorGUILayout.EndHorizontal();
                        break;
                    }
                    if (GUILayout.Button("-", GUILayout.MaxWidth(16f)))
                    {
                        obj.settings.RemoveAt(i);
                        break;
                    }
                }
                EditorGUILayout.EndHorizontal();
            }

            serializedObject.ApplyModifiedProperties();
        }

    }
}
#endif