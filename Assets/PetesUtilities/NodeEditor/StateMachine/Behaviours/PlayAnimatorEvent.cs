using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateBehaviours.Animation
{
	public class PlayAnimatorEvent : StateBehaviour, IEnterState, IUpdateState
	{
        public float Delay;

        public AnimatorEvent Type;

        float enterTime;
        bool played;
		public void EnterState ()
		{
            played = false;
            enterTime = Time.time;
		}

        public void UpdateState()
        {
            if (!played && Delay < (Time.time - enterTime))
            {
                played = true;
                this.SendEvent(this, Type);
            }
        }
	}
}

