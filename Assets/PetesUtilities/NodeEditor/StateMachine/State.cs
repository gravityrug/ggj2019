﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachineNode
{
	[Attributes.NodeColor(0f, .8f, .1f)]
	public class State : Node, ICurrentState
	{
        public bool Exit;

        void Awake()
        {
            stateEntires = GetComponents<IEnterState>();
            stateUpdates = GetComponents<IUpdateState>();
            stateExits = GetComponents<IExitState>();
            fixedUpdates = GetComponents<IFixedState>();

#if UNITY_EDITOR
            foreach (var transition in Transitions)
                CheckTransitionIntegrity(transition);
            foreach (var transition in SubTransitions)
                CheckTransitionIntegrity(transition);
#endif
        }
        /// <summary>
        /// State Properties
        /// </summary>
        [Attributes.NodeField]
        public List<Transition> Transitions = new List<Transition>();

        IEnterState[] stateEntires;
        IUpdateState[] stateUpdates;
        IFixedState[] fixedUpdates;
        IExitState[] stateExits;
        
        /// <summary>
        /// SubState Properties
        /// </summary>
        [Attributes.NodeField(ShowNodeField.BaseNodeOnly)]
		public List<Transition> SubTransitions = new List<Transition>();

        [Attributes.DisableEditing]
        [Attributes.HighLightNode]
		public State currentState;

        public State CurrentState()
        {
            return currentState;
        }

        public void EnterState ()
		{
            Exit = false;

            // Enter State
            foreach (var transition in Transitions)
            {
                transition.Initialize();
            }

            foreach (var entry in stateEntires)
                entry.EnterState();

            // Enter SubState
            foreach (var transition in SubTransitions)
			{
                transition.Initialize();
			}

			foreach(var transition in SubTransitions)
			{
				if (transition.Evaluate())
				{
					currentState = transition.ToState;
                    currentState.EnterState();
					break;
				}
			}				
		}

#if UNITY_EDITOR
        void CheckTransitionIntegrity(Transition transition)
        {
            if (transition == null)
            {
                var trace = new List<Transform>();
                var currentTransform = transform;
                while (currentTransform.parent != null)
                {
                    trace.Add(currentTransform);
                    currentTransform = currentTransform.parent;
                }
                trace.Add(currentTransform);

                var stringBuilder = new System.Text.StringBuilder();
                stringBuilder.Append("Null Transition : ");
                for (int i = trace.Count - 1; i >= 0; i--)
                {
                    if (i != trace.Count - 1)
                        stringBuilder.Append("/");
                    stringBuilder.Append(trace[i].name);
                }
                Debug.LogErrorFormat(this, stringBuilder.ToString());
                Debug.Break();
            }
        }
#endif
        public void FixedState()
        {
            foreach (var state in fixedUpdates)
            {
                state.FixedUpdateState();
            }

            if (currentState)
                currentState.FixedState();
        }

        public void UpdateState ()
		{
            //Update State
            foreach (var state in stateUpdates)
            {
                state.UpdateState();
            }

            //Update Substate
            if (currentState)
            {
                currentState.UpdateState();
                foreach (var transition in currentState.Transitions)
                {
                    if (transition.Evaluate())
                    {
                        currentState.ExitState();
                        currentState = transition.ToState;
                        currentState.EnterState();
                        return;
                    }
                }
            }
            else
            {
                foreach (var transition in SubTransitions)
                {
                    if (transition.Evaluate())
                    {
                        currentState = transition.ToState;
                        currentState.EnterState();
                        break; 
                    }
                }
            }         
        }

		public void ExitState ()
		{
            // Exit SubStates
            if (currentState)
				currentState.ExitState();
            currentState = null;

            foreach (var transition in SubTransitions)
            {
                transition.CleanUp();
            }

            //Exit State
            foreach (var exit in stateExits)
            {
                exit.ExitState();
            }
            foreach (var transition in Transitions)
            {
                transition.CleanUp();
            }
        }
    }
}

#if UNITY_EDITOR
namespace Inspectors
{
    using StateMachineNode;
    using UnityEditor;

    [CustomEditor(typeof(State), true)]
    class StateInspector : Editor, INodeEditor
    {
        DrawAddComponentFields<StateBehaviour> StateBehaviours;

        void OnEnable()
        {
            StateBehaviours = new DrawAddComponentFields<StateBehaviour>(typeof(State), target, serializedObject);
        }

        public void OnNodeGUI()
        {
            StateBehaviours.OnNodeGUI();
        }
    }
}
#endif