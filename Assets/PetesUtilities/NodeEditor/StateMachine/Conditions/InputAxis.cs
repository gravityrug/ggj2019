using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions.Input
{
	public class InputAxis : Condition
	{
        ControllerInput input = null;

        private void Start()
        {
            input = transform.root.GetOrAddComponent<ControllerInput>();
        }

        public Inputs.Axis axis;
        public Conditional condition;
        [Range(-1f, 1f)]
        public float value;

        public override bool Evaluate()
        {
            var axisValue = input.GetAxis(axis);

            switch (condition)
            {
                case Conditional.GreaterThan:
                    return axisValue > value;
                case Conditional.LessThan:
                    return axisValue < value;
                default:
                    return true;
            }
        }
    }
}
