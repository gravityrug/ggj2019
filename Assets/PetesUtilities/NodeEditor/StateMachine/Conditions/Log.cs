using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions.Misc
{
	public class Log : Condition, IHelpInfo
	{
        public string Info;
        public override bool Evaluate()
        {
#if UNITY_EDITOR
            Debug.Log(Info);
#endif
            return true;
        }

        public string HelpInfo()
        {
            return "Returns True and Logs Info Message to the console";
        }
    }
}


