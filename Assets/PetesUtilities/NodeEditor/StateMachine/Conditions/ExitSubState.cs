using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif 


namespace Conditions.State
{
    public class ExitSubState : Condition, IHelpInfo
    {
        private void Start()
        {
            currentState = transform.parent.GetComponent<StateMachineNode.ICurrentState>();
        }

        StateMachineNode.ICurrentState currentState;

        public override bool Evaluate()
        {
            if (currentState == null)
            {
                Debug.LogErrorFormat(this, string.Format("{0} : {1} cannot find parent current State", name, transform.root.name));
                return false;
            }
            return currentState.CurrentState().Exit == true;
        }

        public string HelpInfo()
        {
            return "Returns true if the currently evaluated state exit's it's substate";
        }
    }
}