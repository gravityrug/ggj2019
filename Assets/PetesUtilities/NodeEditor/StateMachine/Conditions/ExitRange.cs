using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions.State
{
	public class ExitRange : Condition, IBeginConditionCheck, IHelpInfo
	{
        public float minWait = 0f, maxWait = 5f;

        public override bool Evaluate()
        {
            var elapsedTime = Time.time - enterTime;
            return elapsedTime > target;
        }

        float enterTime;
        float target;
        public void BeginCheck()
        {
            target = Random.Range(minWait, maxWait);
            enterTime = Time.time;
        }

        public string HelpInfo()
        {
            return "Returns true after a random time between min and Max Values";
        }
    }
}


