using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions.Misc
{
	public class OnAnimatorEvent : Condition, IBeginConditionCheck, IEndConditionCheck, IHelpInfo, IEvent<AnimatorEvent>
	{
        bool eventFired;

        [Range(0f, 5f)]
        [Tooltip("Delays returning true until specified time has passed since event")]
        public float delay = 0f;

        [Attributes.DisableEditing(setting = Attributes.DisableEditing.Setting.PlayModeOnly)]
        public AnimatorEvent Event;

        float EventTimeStamp;
                
        public void BeginCheck()
        {
            this.StartEventCallBacks<AnimatorEvent>();
            eventFired = false;
        }
        
        public void EndCheck()
        {
            this.StopEventCallbacks<AnimatorEvent>();
        }
        
        public override bool Evaluate()
        {
            if (eventFired)
            {
                return Time.time > EventTimeStamp + delay;
            }
            return false;
        }

        public string HelpInfo()
        {
            return "Returns true if event was fired during this state";
        }

        public void OnEvent(AnimatorEvent args)
        {
            if (args == Event)
            {
                EventTimeStamp = Time.time;
                eventFired = true;
            }
        }
    }
}
