using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions.State
{
	public class ExitTime : Condition, IBeginConditionCheck
	{
        [Range(0f,10f)]
        public float Delay;

        float TimeEntered;

        public void BeginCheck()
        {
            TimeEntered = Time.time;
        }

        public override bool Evaluate()
        {
            return Time.time - TimeEntered > Delay;
        }
    }
}
