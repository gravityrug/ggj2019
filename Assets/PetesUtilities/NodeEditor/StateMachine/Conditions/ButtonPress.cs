using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions.Input
{
	public class ButtonPress : Condition
	{
        ControllerInput input;

        private void Awake()
        {
            input = transform.root.GetComponentInChildren<ControllerInput>();
        }

        public Inputs.Buttons button;
        public Inputs.State type;

        public override bool Evaluate()
        {
            return input.GetButton(button) == type;
        }
    }
}
