using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions.State
{
    using StateMachineNode;

	public class CurrentState : Condition, IHelpInfo
	{
        public State state;
        ICurrentState parent;

        private void Start()
        {
            parent = state.transform.parent.GetComponent<ICurrentState>();
        }

        public override bool Evaluate()
        {
            return parent.CurrentState() == state;
        }

        public string HelpInfo()
        {
            return "Returns true if the StateMachine is currently Updating named State";
        }
    }
}

#if UNITY_EDITOR
namespace Inspectors
{
    using UnityEditor;
    using Conditions.State;
    using StateMachineNode;
    using System.Linq;

    [CustomEditor(typeof(CurrentState))]
    class IsStateInspector : Editor
    {
        List<StateMachine> stateMachines = new List<StateMachine>();
        List<StateInfo> baseNodes;
        CurrentState condition;


        class StateInfo
        {
            public Node BaseNode;
            public string BaseNodeName;
            public List<State> States = new List<State>();
        }

        private void OnEnable()
        {
            condition = target as CurrentState;
            var actor = condition.transform.root;
            var statemachines = actor.GetComponentsInChildren<StateMachine>();
            foreach (var stateMachine in statemachines)
            {
                this.stateMachines.Add(stateMachine);
            }
            if (condition.state)
                currentStateMachine = condition.state.GetComponentInParent<StateMachine>();
            UpdateStates();
        }

        StateMachine currentStateMachine;
        private void UpdateStates()
        {
            baseNodes = new List<StateInfo>();

            if (currentStateMachine == null)
                return;

            List<State> machineStates = new List<State>();
            foreach (Transform node in currentStateMachine.transform)
            {
                var state = node.GetComponent<State>();
                if (state != null)
                    machineStates.Add(state);
            }
            if (machineStates.Count > 0)
            {
                var StateInfo = new StateInfo();
                StateInfo.BaseNode = currentStateMachine;
                StateInfo.BaseNodeName = currentStateMachine.name;
                StateInfo.States = machineStates;
                baseNodes.Add(StateInfo);
            }
            
            var subStates = currentStateMachine.GetComponentsInChildren<State>();
            foreach (var substate in subStates)
            {
                List<State> states = new List<State>();
                foreach (Transform node in substate.transform)
                {
                    var state = node.GetComponent<State>();
                    if (state != null)
                        states.Add(state);
                }
                if (states.Count > 0)
                {
                    var info = new StateInfo();
                    info.BaseNode = substate;
                    info.States = states;

                    List<string> path = new List<string>();
                    path.Add(substate.name);
                    var parent = substate.transform.parent;
                    while (parent != currentStateMachine.transform)
                    {
                        path.Add(parent.name);
                        parent = parent.parent;
                    }
                    path.Reverse();
                    string name = "";
                    foreach (var item in path)
                        name += item + "/";
                    name = name.Remove(name.Length - 1);
                    info.BaseNodeName = name;

                    baseNodes.Add(info);
                }                
            }
        }
                
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (stateMachines.Count == 0)
            {
                EditorGUILayout.HelpBox("No Statemachine on Actor", MessageType.Error);
                return;
            }

            int selectedStateMachine = 0;
            for (int i = 0; i < stateMachines.Count; ++i)
            {
                if (stateMachines[i] == currentStateMachine)
                {
                    selectedStateMachine = i;
                    break;
                }
            }

            if (stateMachines.Count > 1)
                selectedStateMachine = EditorGUILayout.Popup("State Machine", selectedStateMachine, stateMachines.Select(x => x.name).ToArray());
            if (currentStateMachine != stateMachines[selectedStateMachine])
            {
                currentStateMachine = stateMachines[selectedStateMachine];
                UpdateStates();
            }
            
            var baseNode = condition.state == null ? null : condition.state.transform.parent.GetComponent<Node>();

            int selectedBaseNode= 0;
            for (int i = 0; i < baseNodes.Count; ++i)
            {
                if (baseNodes[i].BaseNode == baseNode)
                {
                    selectedBaseNode = i;
                    break;
                }
            }

            selectedBaseNode = EditorGUILayout.Popup("Base State", selectedBaseNode, baseNodes.Select(x => x.BaseNodeName).ToArray());
            baseNode = baseNodes[selectedBaseNode].BaseNode;
                        
            int selectedState = 0;
            var states = baseNodes[selectedBaseNode].States;
            for (int i = 0; i < states.Count; ++i)
            {
                if (states[i] == condition.state)
                {
                    selectedState = i;
                    break;
                }
            }

            selectedState = EditorGUILayout.Popup("State", selectedState, states.Select(x => x.name).ToArray());
            var update = condition.state != states[selectedState];
            condition.state = states[selectedState];

            if (update)
                target.GetType().GetMethod("Start", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).Invoke(target, null);


            serializedObject.ApplyModifiedProperties();
        }
    }
}


#endif
