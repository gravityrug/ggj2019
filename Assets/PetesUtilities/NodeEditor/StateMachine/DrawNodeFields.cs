﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Reflection;
using System;

#if UNITY_EDITOR
namespace Inspectors
{
    public class DrawAddComponentFields<T> where T : MonoBehaviour
    {
        public DrawAddComponentFields(Type targetType, UnityEngine.Object target, SerializedObject serializedObject, params Type[] excludeTypes)
        {
            this.targetType = targetType;
            this.target = target;
            this.serializedObject = serializedObject;

            node = target as Node;
            Selection = typeof(Node).Assembly.GetTypes().Where(x => typeof(T).IsAssignableFrom(x) && !x.IsAbstract && excludeTypes.Where(y=> y.IsAssignableFrom(x)).Count() == 0 ).ToList();
            Selection.Sort((x, y) => x.ToString().CompareTo(y.ToString()));
        }

        Type targetType;
        UnityEngine.Object target;
        SerializedObject serializedObject;

        Dictionary<T, Editor> editors = new Dictionary<T, Editor>();
        Dictionary<int, bool> helpLookup = new Dictionary<int, bool>();
        Node node;

        public List<Type> Selection = new List<Type>();
        Vector2 scrollPosition;

        public void OnNodeGUI()
        {
            if (!node)
                return;

            var style = new GUIStyle(GUI.skin.button);
            style.fontStyle = FontStyle.Bold;
            style.fontSize = 14;
            EditorGUILayout.LabelField("", GUILayout.Height(2f));
            string name = typeof(T).Name.AddSpaceBeforeCaps();
            if (GUILayout.Button(new GUIContent(string.Format("Add {0}", name), string.Format("Adds a {0} to Node", name)), style, GUILayout.Height(24f)))
            {
                DrawAddComponentMenu();
            }

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
            {
                var allComponents = node.GetComponents<T>();
                serializedObject.Update();
                for (int i = 0; i < allComponents.Length; ++i)
                {
                    var component = allComponents[i];
                    var first = i == 0;
                    var last = i == allComponents.Length - 1;

                    DrawComponent(component, first, last);
                }
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndScrollView();
        }

        void DrawAddComponentMenu()
        {
            GenericMenu menu = new GenericMenu();

            foreach (var type in Selection)
            {
                //var name = type.ToString();//.Replace( , "").Replace(".", "/");
                var path = type.ToString().Split(new char[] { '.' });
                string name = "";
                if (path.Length == 1)
                    name = path[0];
                else
                {
                    for (int i = 1; i < path.Length; ++i)
                    {
                        name += path[i];
                        if (i != path.Length - 1)
                            name += "/";
                    }
                }

                menu.AddItem(new GUIContent(name), false, () => Add(type));
            }
            menu.ShowAsContext();
        }

        void DrawComponent(T component, bool first, bool last)
        {
            bool showHelp = false;
            Editor editor;
            if (!editors.TryGetValue(component, out editor))
            {
                editor = Editor.CreateEditor(component);
                editors.Add(component, editor);
            }
            {
                helpLookup.TryGetValue(component.GetInstanceID(), out showHelp);
            }

            bool delete = false;
            EditorGUILayout.BeginHorizontal();
            {
                var style = new GUIStyle(EditorStyles.helpBox);
                style.alignment = TextAnchor.MiddleCenter;
                style.fontStyle = FontStyle.Bold;
                style.fontSize = 12;
                var width = 20f;
                var height = 20f;

                style.normal.textColor = Color.white;
                var defaultBG = GUI.backgroundColor;

                GUI.backgroundColor = new Color(.1f, .1f, .1f);

                if (first)
                    GUI.enabled = false;
                if (GUILayout.Button("↑", style, GUILayout.Width(width), GUILayout.Height(height)))
                {
                    UnityEditorInternal.ComponentUtility.MoveComponentUp(component);
                }
                GUI.enabled = true;
                if (last)
                    GUI.enabled = false;
                if (GUILayout.Button("↓", style, GUILayout.Width(width), GUILayout.Height(height)))
                {
                    UnityEditorInternal.ComponentUtility.MoveComponentDown(component);
                }
                GUI.enabled = true;

                string conditionName = component.GetType().Name.AddSpaceBeforeCaps();

                if (GUILayout.Button(conditionName, style, GUILayout.Height(height)))
                {
                    SerializedObject so = new SerializedObject(component);
                    var script = so.FindProperty("m_Script").objectReferenceValue;
                    EditorGUIUtility.PingObject(script);
                }
                //EditorGUILayout.LabelField(conditionName, style, GUILayout.Height(height));
                                
                if (GUILayout.Button(new GUIContent("H", "Shows Help Info"), style, GUILayout.Width(width), GUILayout.Height(height)))
                {
                    helpLookup[component.GetInstanceID()] = !showHelp;
                }

                if (GUILayout.Button(new GUIContent("E", "Open Script Editor"), style, GUILayout.Width(width), GUILayout.Height(height)))
                {
                    SerializedObject so = new SerializedObject(component);
                    var script = so.FindProperty("m_Script").objectReferenceValue;
                    AssetDatabase.OpenAsset(script, 0);
                }

                if (GUILayout.Button(new GUIContent("-", "Remove"), style, GUILayout.Width(width), GUILayout.Height(height)))
                {
                    delete = true;
                    Remove(component);
                }

                GUI.backgroundColor = defaultBG;
            }
            EditorGUILayout.EndHorizontal();
            if (!delete)
            {
                if (showHelp)
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        if (component is StateBehaviour)
                        {
                        if (component is IEnterState)
                            EditorGUILayout.LabelField(new GUIContent("Enter", "Behaviour Runs when entering a State"), EditorStyles.helpBox);
                        if (component is IUpdateState)
                            EditorGUILayout.LabelField(new GUIContent("Update", "Behaviour Runs during Update when in State"), EditorStyles.helpBox);
                        if (component is IFixedState)
                            EditorGUILayout.LabelField(new GUIContent("Fixed", "Behaviour Runs during Fixed Update when in State"), EditorStyles.helpBox);
                        if (component is IExitState)
                            EditorGUILayout.LabelField(new GUIContent("Exit", "Behaviour Runs when exiting a State"), EditorStyles.helpBox);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                    if (component is IHelpInfo)
                        EditorGUILayout.HelpBox((component as IHelpInfo).HelpInfo(), MessageType.Info);
                }
                editor.OnInspectorGUI();
            }    
            EditorGUILayout.LabelField(GUIContent.none);
        }

        void Add(Type type)
        {
            var component = node.gameObject.AddComponent(type) as T;
            editors.Add(component, Editor.CreateEditor(component));
            ReInitialize();
        }

        void Remove(T component)
        {
            Editor editor;
            editors.TryGetValue(component, out editor);
            if (editor) Editor.DestroyImmediate(editor);
            editors.Remove(component);
            Editor.DestroyImmediate(component);
            ReInitialize();
        }

        public virtual void ReInitialize()
        {
            var InitMethod = targetType.GetMethod("Awake", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            if (InitMethod != null) InitMethod.Invoke(target, null);
        }
    }
}

#endif