﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class AddConditionScript: EditorWindow
{
	[MenuItem("Assets/Create/Scripts/Condition", false, -51)]
	static void Init()
	{
		var window = EditorWindow.GetWindow(typeof(AddConditionScript));
		window.Show();
	}

	string newName;

	void Awake()
	{
		awake = false;
	}
	bool awake;

    void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        GUI.SetNextControlName("MyInput");
        newName = EditorGUILayout.TextField(newName);

        if (!awake)
        {
            GUI.FocusControl("MyInput");
            position = new Rect(GUIUtility.GUIToScreenPoint(Event.current.mousePosition), new Vector2(600, EditorGUIUtility.singleLineHeight));
            awake = true;
        }

        if (!string.IsNullOrEmpty(newName))
        {
            if (GUILayout.Button("Create") || Event.current.keyCode == KeyCode.Return)
            {
                CreateAsset(newName);
                Close();
            }
        }
        else
        {
            GUI.enabled = false;
            GUILayout.Button("Create");
            GUI.enabled = true;
        }

        if (GUILayout.Button("Close"))
            Close();
        EditorGUILayout.EndHorizontal();
    }

    void CreateAsset(string name)
    {
        var path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (!AssetDatabase.IsValidFolder(path))
            path = "Assets";

        path += "/" + name + ".cs";

        var asset = Resources.Load("ConditionScript") as TextAsset;
        var txt = asset.text.Replace("####", name);

        File.WriteAllText(path, txt);

        AssetDatabase.Refresh();
        var obj = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
        EditorGUIUtility.PingObject(obj);
    }
}
