﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inspectors
{
    using UnityEditor;

    [CustomEditor(typeof(StateBehaviours.Misc.ExitSubState))]
    class ExitSubStateInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            var obj = target as MonoBehaviour;
            if (obj == null) return;
            if (obj.transform.parent == null || obj.transform.parent.GetComponent<StateMachineNode.State>() == null)
                EditorGUILayout.HelpBox("BASE STATE NEEDS TO BE A STATE", MessageType.Error);
        }
    }
}

