﻿public interface IEnterState
{
	void EnterState();
}

public interface IExitState
{
	void ExitState();
}

public interface IUpdateState
{
	void UpdateState();
}

public interface IFixedState
{
    void FixedUpdateState();
}