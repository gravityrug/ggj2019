﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Condition : MonoBehaviour 
{
	/// <summary>
	/// Called only when condition is Evaluated
    /// Won't be called if an earlier condition failed
	/// </summary>
	public virtual bool Evaluate()
	{
		return false;
	}
}

/// <summary>
/// When implemented on a condition, is called once before the condition begins to evaluate
/// </summary>
public interface IBeginConditionCheck
{
    void BeginCheck();
}

/// <summary>
/// When implemented on a condition, is called once after there's no need to evaluate the condition further
/// </summary>
public interface IEndConditionCheck
{
    void EndCheck();
}

public enum Conditional
{
	GreaterThan,
	LessThan
}

public enum EqualityConditional
{
	GreaterThan,
	LessThan,
	EqualTo
}

#if UNITY_EDITOR
namespace Inspectors
{
    using UnityEditor;

    [CustomEditor(typeof(Condition), true)]
    class ConditionInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject,  "m_Script");
            serializedObject.ApplyModifiedProperties();
        }
    }
}

#endif