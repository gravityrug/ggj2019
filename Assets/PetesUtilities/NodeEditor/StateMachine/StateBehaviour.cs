﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateBehaviour : MonoBehaviour 
{

}

#if UNITY_EDITOR
namespace Inspectors
{
    using UnityEditor;

    [CustomEditor(typeof(StateBehaviour), true)]
    class StateBehaviourInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script");
            serializedObject.ApplyModifiedProperties();
        }
    }
}

#endif