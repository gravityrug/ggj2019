using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

namespace Inputs
{
    public enum Axis
    {
        LeftStickX,
        LeftStickY,
        LeftStickTilt,

        RightStickX,
        RightStickY,
        RightStickTilt,

        LeftTrigger,
        RightTrigger
    }

    public enum Buttons
    {
        DpadUp,
        DpadDown,
        DpadLeft,
        DpadRight,

        Square,
        Triangle,
        Cross,
        Circle,

        LeftShoulder,
        RightShoulder,

        LeftTrigger,
        RightTrigger,

        LeftHat,
        RightHat,

        Start,
        Select
    }

    public enum State
    {
        None = 0,
        Pushed,
        Held,
        Released
    }

    public enum PlayerIndex
    {
        One = 0,
        Two = 1,
        Three = 2,
        Four = 3,
        FirstConnected = 4,
        Any = 5
    }
}

public class ControllerInput : MonoBehaviour
{
    public Inputs.PlayerIndex controller = Inputs.PlayerIndex.FirstConnected;

    public float GetAxis(Inputs.Axis axis)
    {
        if (!enabled)
            return 0f;
        return ControllerInputManager.Get.controllers[(int)controller].GetAxis(axis);
    }

    public Inputs.State GetButton(Inputs.Buttons button)
    {
        if (!enabled)
            return Inputs.State.None;

        return ControllerInputManager.Get.controllers[(int)controller].GetButton(button);
    }

    public void SetRumble(float leftMotor, float rightMotor)
    {
        var gamepad = ControllerInputManager.Get.controllers[(int)controller];
        gamepad.leftMotorVibration = leftMotor;
        gamepad.rightMotorVibration = rightMotor;
        gamepad.updateMotors = true;
    }

    public bool Connected
    {
        get { return ControllerInputManager.Get.controllers[(int)controller].enabled; }
    }
}

public class ControllerInputManager : Manager<ControllerInputManager>
{
    public class ControllerState
    {
        public ControllerState()
        {
            AxisValues = new float[Enum.GetValues(typeof(Inputs.Axis)).Length];
            ButtonValues = new Inputs.State[Enum.GetValues(typeof(Inputs.Buttons)).Length];
        }

        public bool enabled;
        public bool updateMotors = true;
        public float leftMotorVibration;
        public float rightMotorVibration;
        public float[] AxisValues;
        public Inputs.State[] ButtonValues;

        public Inputs.State GetButton(Inputs.Buttons button)
        {
            return ButtonValues[(int)button];
        }

        public float GetAxis(Inputs.Axis axis)
        {
            return AxisValues[(int)axis];
        }

        public void SetAxis(Inputs.Axis axis, float value)
        {
            AxisValues[(int)axis] = Mathf.Clamp(value, -1f, 1f);
        }

        public void SetButton(Inputs.Buttons button, bool pressed)
        {
            var buttonIndex = (int)button;
            var type = ButtonValues[buttonIndex];
            if (pressed)
            {
                if (type == Inputs.State.Held || type == Inputs.State.Pushed)
                    ButtonValues[buttonIndex] = Inputs.State.Held;
                else
                    ButtonValues[buttonIndex] = Inputs.State.Pushed;
            }
            else
            {
                if (type == Inputs.State.Held || type == Inputs.State.Pushed)
                    ButtonValues[buttonIndex] = Inputs.State.Released;
                else
                    ButtonValues[buttonIndex] = Inputs.State.None;
            }
        }
    }

    public ControllerState[] controllers = new ControllerState[]
    {
        new ControllerState(),
        new ControllerState(),
        new ControllerState(),
        new ControllerState(),
        new ControllerState(),
        new ControllerState()
    };

    private void Awake()
    {
        DontDestroyOnLoad(this);
        Update();
    }

    private void Update()
    {
        int firstConnected = -1;
        for (int i  = 0; i < 6; ++ i)
        {
            if (i == 4)
            {
                if (firstConnected >= 0)
                    controllers[i] = controllers[firstConnected];
                continue;
            }

            if (i == 5)
            {
                if (firstConnected >= 0)
                {
                    controllers[i].enabled = true;
                    UpdateAnyControllerState();
                }
                else
                {
                    controllers[i].enabled = false;
                    controllers[i] = new ControllerState();
                }
                continue;
            }
            
            var controller = controllers[i];

            GamePadState state = GamePad.GetState((PlayerIndex)i);
            if (!state.IsConnected)
            {
                if (controller.enabled)
                {
                    controllers[i] = new ControllerState();
                }
                continue;
            }
            if (firstConnected < 0)
                firstConnected = i;

            controller.enabled = true;

            float lx = state.ThumbSticks.Left.X;
            float ly = state.ThumbSticks.Left.Y;
            float rx = state.ThumbSticks.Right.X;
            float ry = state.ThumbSticks.Right.Y;

            if (i < 4)
            {
                if (controller.updateMotors)
                {
                    controller.updateMotors = false;
                    XInputDotNetPure.GamePad.SetVibration((PlayerIndex)i, controller.leftMotorVibration, controller.rightMotorVibration);
                }
            }

            controller.SetAxis(Inputs.Axis.LeftStickX, lx);
            controller.SetAxis(Inputs.Axis.LeftStickY, ly);

            controller.SetAxis(Inputs.Axis.RightStickX, rx);
            controller.SetAxis(Inputs.Axis.RightStickY, ry);

            float leftTilt = new Vector2(lx * Mathf.Sqrt(1 - .5f * ly * ly), ly * Mathf.Sqrt(1 - .5f * lx * lx)).magnitude;
            float rightTilt = new Vector2(rx * Mathf.Sqrt(1 - .5f * ry * ry), ry * Mathf.Sqrt(1 - .5f * rx * rx)).magnitude;

            controller.SetAxis(Inputs.Axis.LeftStickTilt, leftTilt);
            controller.SetAxis(Inputs.Axis.RightStickTilt, rightTilt);

            controller.SetAxis(Inputs.Axis.LeftTrigger, state.Triggers.Left);
            controller.SetAxis(Inputs.Axis.RightTrigger, state.Triggers.Right);

            controller.SetButton(Inputs.Buttons.LeftTrigger, state.Triggers.Left > .3f);
            controller.SetButton(Inputs.Buttons.RightTrigger, state.Triggers.Right > .3f);

            controller.SetButton(Inputs.Buttons.RightShoulder, state.Buttons.RightShoulder == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.LeftShoulder, state.Buttons.LeftShoulder == ButtonState.Pressed);

            controller.SetButton(Inputs.Buttons.LeftHat, state.Buttons.LeftStick == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.RightHat, state.Buttons.RightStick == ButtonState.Pressed);

            controller.SetButton(Inputs.Buttons.Cross, state.Buttons.A == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.Circle, state.Buttons.B == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.Square, state.Buttons.X == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.Triangle, state.Buttons.Y == ButtonState.Pressed);

            controller.SetButton(Inputs.Buttons.Start, state.Buttons.Start == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.Select, state.Buttons.Back == ButtonState.Pressed);

            controller.SetButton(Inputs.Buttons.DpadUp, state.DPad.Up == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.DpadDown, state.DPad.Down == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.DpadLeft, state.DPad.Left == ButtonState.Pressed);
            controller.SetButton(Inputs.Buttons.DpadRight, state.DPad.Right == ButtonState.Pressed);
        }
    }

    private void OnDestroy()
    {
        foreach (var index in EnumUtility.GetValues<PlayerIndex>())
        {
            GamePad.SetVibration(index, 0f, 0f);
        }
    }

    void UpdateAnyControllerState()
    {
        var controller = controllers[5];
        foreach (var button in EnumUtility.GetValues<Inputs.Buttons>())
        {
            bool pressed = false;
            for (int i = 0; i < 4; ++i)
            {
                var value = controllers[i].GetButton(button);
                if (value == Inputs.State.Pushed || value == Inputs.State.Held)
                {
                    pressed = true;
                    break;
                }
            }
            controller.SetButton(button, pressed);
        }

        foreach (var axis in EnumUtility.GetValues<Inputs.Axis>())
        {
            if (axis == Inputs.Axis.LeftStickTilt || axis == Inputs.Axis.RightStickTilt)
            {
                var lx = axis == Inputs.Axis.LeftStickTilt ? controller.GetAxis(Inputs.Axis.LeftStickX) : controller.GetAxis(Inputs.Axis.RightStickX);
                var ly = axis == Inputs.Axis.LeftStickTilt ? controller.GetAxis(Inputs.Axis.LeftStickY) : controller.GetAxis(Inputs.Axis.RightStickY);
                controller.SetAxis(axis, new Vector2(lx * Mathf.Sqrt(1 - .5f * ly * ly), ly * Mathf.Sqrt(1 - .5f * lx * lx)).magnitude);
                continue;
            }

            if (axis == Inputs.Axis.LeftTrigger || axis == Inputs.Axis.RightTrigger)
            {
                float highest = 0;
                for (int i = 0; i < 4; ++i)
                {
                    var newValue = controllers[i].GetAxis(axis);
                    if (newValue > highest)
                        highest = newValue;
                }
                controller.SetAxis(axis, highest);
                continue;
            }

            float value = 0;
            for (int i = 0; i < 4; ++i)
            {
                value += controllers[i].GetAxis(axis);
            }
            controller.SetAxis(axis, Mathf.Clamp(value, -1f, 1f));

        }
    }
}

#if UNITY_EDITOR
namespace Inspectors
{
    using UnityEditor;

    [CustomEditor(typeof(ControllerInputManager))]
    class ControllerInputManagerInspector : Editor
    {
        static Inputs.PlayerIndex playerIndex = Inputs.PlayerIndex.One;

        public override void OnInspectorGUI()
        {

            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script");
            serializedObject.ApplyModifiedProperties();

            var manager = (ControllerInputManager)target;

            playerIndex = (Inputs.PlayerIndex)EditorGUILayout.EnumPopup("Inspected Controller", playerIndex);
            foreach (Inputs.PlayerIndex index in Enum.GetValues(typeof(PlayerIndex)))
            {
                if (index == Inputs.PlayerIndex.FirstConnected)
                    continue;

                EditorGUILayout.LabelField(string.Format("Player Index {0} {1}", (int)index + 1, 
                manager.controllers[(int)index].enabled ? "Connected" : "Disconnected"));
            }

            EditorGUILayout.LabelField("");

            EditorGUILayout.LabelField("Axis", EditorStyles.boldLabel);
            
            EditorGUI.indentLevel++;

            foreach (Inputs.Axis axis in System.Enum.GetValues(typeof(Inputs.Axis)))
            {
                EditorGUILayout.LabelField(string.Format("{1:0.###} \t: {0}", axis.ToString(), manager.controllers[(int)playerIndex].AxisValues[(int)axis]));
            }

            EditorGUI.indentLevel--;

            EditorGUILayout.LabelField("");
            EditorGUILayout.LabelField("Buttons", EditorStyles.boldLabel);

            EditorGUI.indentLevel++;

            foreach (Inputs.Buttons button in System.Enum.GetValues(typeof(Inputs.Buttons)))
            {
                EditorGUILayout.LabelField(string.Format("{1} \t: {0}", button.ToString(), manager.controllers[(int)playerIndex].ButtonValues[(int)button]));
            }

            EditorGUI.indentLevel--;
        }

        public override bool RequiresConstantRepaint()
        {
            return true;
        }
    }

    [CustomEditor(typeof(ControllerInput))]
    class ControllerInputInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script");
            serializedObject.ApplyModifiedProperties();

            if (Application.isPlaying)
            {
                var controller = (ControllerInput)target;

                EditorGUILayout.LabelField("Axis", EditorStyles.boldLabel);
                EditorGUI.indentLevel++;
                foreach (Inputs.Axis axis in System.Enum.GetValues(typeof(Inputs.Axis)))
                {
                    EditorGUILayout.LabelField(string.Format("{1:0.###} \t: {0}", axis.ToString(), controller.GetAxis(axis)));
                }

                EditorGUI.indentLevel--;

                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField("Buttons", EditorStyles.boldLabel);

                EditorGUI.indentLevel++;

                foreach (Inputs.Buttons button in System.Enum.GetValues(typeof(Inputs.Buttons)))
                {
                    EditorGUILayout.LabelField(string.Format("{1} \t: {0}", button.ToString(), controller.GetButton(button).ToString()));
                }
                EditorGUI.indentLevel--;
            }   
        }


        public override bool RequiresConstantRepaint()
        {
            return true;
        }
    }
}
#endif