using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Conditions
{
	public class HoldingCat : Condition
	{
        CatLadyData data;
        private void Start()
        {
            data = this.GetFromHeirarchy<CatLadyData>();
        }

        public override bool Evaluate()
        {
            return data.HeldObject.GetFromHeirarchy<CatSystem>();
        }
    }
}


